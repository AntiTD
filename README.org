#+TITLE:     Spec/plan laboration 4, apjava
#+AUTHOR:    Anton Johansson, Andreas Jacobsson
#+EMAIL:     anton.johansson@gmail.com
#+DATE:      2008-12-08 Mon

* Plan / Tankar
  Se [[file:report/timplan.xsl][timplan.xsl]]
  - Trådosäkert att lägga till torn från en anan tråd.
  - repaint() är trådsäker.
  - Synkronisera gemensam variabel? agents?
  - Synkronisera HashMap i PrototypeFactory?
  - Systembeskrivningen är extra viktig i denna labb. Hur kan
    programmet utökas och vidareutvecklas.
* Frågor
** DONE Trådar
   Vad skulle fördelen kunna vara med att försöka sätta varje Unit i
   en separat tråd? Vad blir mest beräkningstungt, att göra massa
   saker i samma tråd mot att hantera varje enhets logik separat?

*** Svar: Vi väljer inte separata trådar för varje enhet, detta eftersom det verkar bli mer problem.
   
** DONE Statiska metoder vs andra
   Hur ska man på ett bra sätt implementerar Builder/Factory klasser?
   Vad är fördelen/nackdelen med att dessa enbart innehåller statiska
   metoder om så är möjligt.

** DONE Interface som extendar annat interface, se UML.
   Svar: Detta är okej, nyckelordet är extends men det betyder exakt
   samma sak som implements.
** DONE Protected attribut
   Känns farligt att använda men när ska man göra det? Om det inte
   krävs någon koll för get och set kan det väl lika gärna vara
   protected?

   Svar: Kör inte med det! Bra för framtiden att kunna lägga till
   kontroller, t ex positioner ska inte kunna vara negativa.
** TODO ImageObserver?
   Hur är det tänkt att det ska användas, t ex om man vill kolla
   getWidth(ImageObserver) på en BufferedImage.
** TODO Decorator at runtime för att dekorera Agents?
** DONE Observer, observerar den automatiskt sig själv?
   Eftersom objekten klonas så kommer de ha referenser till samma
   listor, d v s sista Observern lägger till Observers för alla
   föregående.
* Planeringsmöten
** Första
   CLOCK: [2008-12-10 Wed 20:30]--[2008-12-11 Thu 00:36] =>  4:06
   
*** Klasser
    - Item <|- Health, Teleport, SpeedBoost, Guns 'n' ammo.
    - Square <|- Stone, Savanna, Road. Idé, varje Square har en
      instans av Javas Rectangle för att kunna använda dess intersects
      metod.
    - Road <|- Sand, Ice.
    - Intersection en Item? Ett sätt att välja väg vid
      flervalskorsningar.
    - Map, JComponent?
    - Model håller koll på alla enheter.
    - Controller, The allseeing eye, säger till varje enhet att
      uppdatera sig själv.
    - View, ritas upp för varje fram. Kontroller meddelar när det är
      dags, data hämtas från Model.
    - Poängräkning skulle kunna ske i separat tråd som sover
      specificerat antal millisekunder för att sedan vakna upp och
      räkna av antalet enheter på plan.
    - Player, synchronized score!
      
*** Interface
    - Ett interface för att se till att allt som behöver det har en
      =intersects()= metod för att testa om den överlappar annan klass
      som implementerar samma interface. Främst för Unit.
      
*** Logik
   Squares extends JComponent, Observable (Towers ska vara Observers),
   Units meddelar rutor att de befinner sig i dem. Towers får då en
   referns till aktuell Unit och lägger denna i en lista för
   beskjutning.
   
   Anledningen till något typ av arv av JComponent är att
   vi vill ha metoden getComponentAt(Point) från en högre nivå (Map av
   typ JComponent?) för att Units ska kunna veta vilken ruta de
   befinner sig på.

*** Kartor XML
    : <map>
    :  <row>
    :    <square type="stone"/>
    :    <square type="road">
    :      <item type="health"/>
    :    </square>
    :  </row>
    : </map>
   
*** Collision detection
    Varje Unit kollar om den intersectar med någon annan Unit innan
    den rör sig. Om den gör det så minskar den flytten tills den
    hittar en flytt som inte intersectar med någon Unit.

*** Flytt av Unit
    1) Unit leter upp rutan den befinner sig i: Frågar map (JComponent
       getComponentAt(Point?))
    2) Fråga ruta om aktuell speedFactor, för att modifiera sin
       hastighet. 1 är standard, 1 * UnitSpeed.
    3) Unit meddelar sin ruta att den befinner sig där, Obervable
       ruta. Rutan meddelar Tower, Tower sparar Unit i kö. Se [[*Logik]]
    4) Unit har en tidsenhet som intervall på hur ofta den får göra
      förflyttningar, detta representerar hastighet. Unit tidstämplas
      vid varje flytt med aktuell tid i millisekunder. Vid nästa flytt
      kollar den så att satt tidsintervall har förflutit.
    5) Enumeration håller koll på riktning.
    6) Se [[*Collision%20detection][*Collision detection]]
  
  - Vi använder XMLSchema.
    
*** TODO Till nästa gång
**** TODO Ta reda på hur vi svänger i kurvor.
**** TODO Metoderna intersects och contains.
**** TODO Easter eggs?
     
** Andra
   CLOCK: [2008-12-11 Thu 16:07]--[2008-12-11 Thu 19:06] =>  2:59
   
*** Path <|- Turnpath
    Hela Canvas har en mouseListener som lyssnar på musklick, dessa
    koordinater kollas mot matris för spelplanen och objekt som finns
    där returneras, Object instanceof Turnpath ger ändring i sväng.
*** Flyweight pattern för rutor?
*** En matris för spelplanen
    Spelplanen vet koordinater för varje ruta, Units kan fråga
    spelplanen efter ruta för aktuell koordinat!
    
*** DoubleBuffering
    Skapa en instans av bakgrundsbild att rita hämtas till en instans
    av BufferedImage för att rita upp gubbar på för varje
    uppritning. Clear Canvas, rita upp BufferedImage och vipps så är
    där DoubleBuffering.
    
*** Animationtråd
    Tården kan se ut så här:
    : while(running) {
    :     for (Unit unit : units) {
    :         unit.update();
    :     }
    :     for (Unit unit : units) {
    :         unit.paint(Graphics g);
    :     }
    : }
*** Gränssnitt
    Tänk på att knappar kan vara bilder.
*** Ett Game
    - Nytt game startas, spelare finns redan.
    - Karta skapas
    - Pengar ges
    - Poäng är noll
    - Torn placeras
    - Game on!
      
*** Levels.xml
    - Levevels.xml Innerhåller information om hur banan ska byggas
    upp.
    - LevelStyle.xml innerhåller information om vilka bilder som hör
      ihop med vilka ban-element.
    - UnitStyles.xml innehåller information om vilka bilder som hör
      ihop med vilka Units.
      
** Tredje
   CLOCK: [2008-12-18 Thu 14:45]--[2008-12-18 Thu 16:10] =>  1:25
*** DONE Plan för framtida arbeta uppgjord se, [[*Plan][Plan]]

* Implementeringstillfällen
** Första
   CLOCK: [2008-12-14 Sun 14:00]--[2008-12-14 Sun 20:36] =>  6:36
*** DONE Animerade Units
    Units förflyttar sig över skärmen, statisk bakgrundsbild ligger
    kvar, är detta dubbelbuffrat?
*** DONE Början på hela programmets struktur.
   
** Andra
   CLOCK: [2008-12-15 Mon 14:15]--[2008-12-15 Mon ?]
*** DONE Kartan ritas upp med bilder
*** DONE Tower implementerad och utsatt på karta!
*** DONE MouseListener på GameComponent
*** DONE Plan för framtida arbeta initierad se, [[*Plan][Plan]]
** Tredje
   CLOCK: [2008-12-16 Tue 18:30]--[2008-12-16 Tue 23:37] =>  5:07
*** DONE GoalSquare
*** DONE Register Observer, Observable Tower observs PathSquares.

* Om spelet
  Ett Tower Defence-spel är ett spel i vilket användarens uppgift är att
  skydda sig mot fiender genom att sätta upp torn. Dessa torn ska sedan
  skjuta ner fienden innan de når ett givet mål, och tornen måste
  således placeras ut strategiskt.
  
  För att göra spelet intressantare brukar det krävas att en viss mängd
  fienden tar sig i mål för att man ska förlora, och det kan finnas
  flera sorters fiender och torn (flygande och markgående fienden etc.).
  
* Spelets regler
  - Att släppa ut trupper ska kosta valuta (nedan kallat kredit), och
    spelaren får ett antal krediter tilldelade varje bana. När dessa
    är slut kan inga trupper köpas, och spelaren förlorar om de
    trupper på banan inte tar sig i mål och ger upphov till vinst.
  - Vinst är när ett visst antal "målkrediter" tjänats. Detta brukar
    bara vara ett antal truppmedlemmar, men kan mycket väl viktas med
    deras hälsa när de kommer in i mål.
  - Krediter tjänas på något sätt under banans gång, förslagsvis en
    viss krediter/sekund för varje truppmedlem, samt en viss mängd
    krediter vid målgång.
  - Fienden behöver inte vara smarta. Det räcker med att de väljer en
    varelse inom räckhåll och skjuter på den.
  - Fienden får ha endast laser (ljusstråle som träffar direkt så ni
    slipper beräkna en projektils bana).
  - Utplacering av tornen som skjuter på trupperna behöver endast
    placeras ut slumpmässigt i tillåtna zoner.

* Laborationen
  Laborationen ska lösas i grupper om 2 (två) personer. Det ses från vår
  sida som en näst intill omöjlig uppgift att göra laborationen enskilt
  under kursens gång, därför finns det inga undantag. Parindelningen är
  upp till er att fixa, men alla grupper ska anmäla sig på denna
  sida. Vet ni inte om någon att jobba med så skriv in er i en ny grupp
  , eller om det redan finns en grupp med bara en person så skriv in er
  i den och kontakta så snart som möjligt personen som redan stod där om
  att du vill jobba med honnom/henne.

  Målet med spelet är att skapa en bas för utbyggnad av
  funktionaliteten. Därför är det väldigt viktigt att ni lägger stor
  möda på spelets interna struktur för att få en lättnavigerad kodbas
  med logisk uppbyggnad. Dokumentationen är väldigt viktig den med,
  vilket innebär att samtliga klasser, samtliga metoder samt
  medlemsvariabler ska kommenteras med utförliga
  JavaDoc-kommentarer. JavaDoc är dock inte den enda typen av grundlig
  dokumentation som krävs, se nedan.  Syfte

* Syftet med laborationen är att få pröva på och använda:
  - Trådar
  - XML / DTD / XMLSchema
  - GUI (Swing)
  - Grafik
  - Öva färdigheten i rapportskrivning med en specifik målgrupp
  - Följa en given kodkonvention

* Er uppgift
  Er uppgift är inte att implementera ett Tower Defence-spel, utan ett
  Anti-Tower Defence-spel. Ni ska alltså låta användaren skicka ut
  fienden som sedan datorn ska skjuta ner. Ett exempel på ett sådant
  spel är: [[http://www.sugar-free-games.com/playgame.php%3Fgame%3D1127][Anti-TD]] på Sugar Free Games.
** Kravlista
   För att göra spelet mer intressant ut laborationssynpunkt finns det
   en del krav på er lösning:
   
*** I alla nivåer gäller:
**** TODO Implementera spelet AntiTD i Java
**** DONE Följa SUN:s kodkonvention: http://java.sun.com/docs/codeconv/index.html
**** TODO Följa god OO- och MDI-stil.
**** DONE Källkoden skall ligga i katalogen
     : ~/edu/apjava/lab4/src/
**** DONE En komplierad version i katalogen
     : ~/edu/apjava/lab4/bin/
**** DONE Rapporten skall ligga i katalogen
     : ~/edu/apjava/lab4/report/
**** TODO JavaDoc-sidorna skall ligga i katalogen
     : ~/edu/apjava/lab4/doc/
   
*** TODO Nivå 1 (grundnivå)
**** DONE Klassen "AntiTD"
     Startar ett Anti Tower Defence-spel utan att ta emot några argument från användaren.
**** DONE Trådar
     Spelet ska använda sig av flera trådar för att låta flera delar jobba parallellt.
**** DONE Uppdateringsintervall
     Spelets uppdateringsintervall skall vara tidsberoende och inte
     beroende av datorns hastighet.
**** DONE Banor
     Det ska finnas minst en bana längs vilken man kan skicka ut
     trupper.
**** TODO Vinst/Förlust
     Vid "vinst" eller "förlust" så ska användaren presenteras med
     valet om att spela igen eller avsluta spelet. Medans detta val
     pågår så ska inte användaren kunna göra nåt annat, men objekt på
     spelplanen ska fortsätta röra sig.
**** DONE GUI:t skall använda sig av Swing-biblioteket.
**** DONE Rendering
     All rendering skall ske dubbelbuffrat för att undvika flimmer.
**** TODO Det ska finnas minst två huvudmenyer:
     - En meny med valen:
       + DONE New Game/Restart
         + Om inget spel har startats så heter menypunkten tex. "New
           Game" och vid klick så startar den ett nytt spel. Om spelet
           redan är igång så ska menypunkten heta tex. "Restart" och
           spelet börjar om från början.
       + DONE Pause/Resume
         + Menypunkten kan tex. heta "Pause" om spelet är igång och
           när man klickar på menypunkten så ska alla trupper och torn
           stanna och inga knappar ska gå att klicka på. Om spelet är
           i "Pause"-läge så ska menypunkten byta namn till
           tex. "Resume" och vid klick så ska spelet återupptas där
           det sist var, dvs. fordonen skall fortsätta röra på sig
           samt att användaren kan skicka ut fler trupper.
       + TODO Mute
         + Sluta spela upp ljud. Då den här menyn väljs ska
           menyalternativet bytas till "Unmute" och vice versa.
       + DONE Quit
         + Avsluta spelet.
     - DONE Den andra menyn ska innehålla:
       + About: Berättar i en dialogruta vem som gjort spelet och när.
       + Help: Berättar i en dialogruta hur man spelar.
**** DONE Zoner i banor
     Varje bana skall innehålla zoner där datorn kan placera ut torn,
     vägar längs vilka trupper kan röra sig.
**** DONE Grupptyper
     Minst två typer av trupper ska implementeras med olika förmågor
     (klarar olika antal träffar, kostar olika mycket, har olika
     hastighet, etc.).

*** TODO Nivå 2 (multipla banor/XML)
**** DONE Flera banor
     Spelet ska klara av flera banor. Banorna ska se olika ut,
     exempelvis hur trupperna kan röra sig.
**** DONE Spara banor i fil levels.xml
     Flera banor skall lagras i en fil som heter
     "levels.xml". XML-formatet skall följa en DTD som ni själva
     specificerar. XML-filen skall valideras mot den DTD (eller XML
     Schema) ni skriver.
**** DONE Banzoner
     Varje bana skall innehålla zoner där datorn kan placera ut torn,
     vägar längs vilka trupper kan röra sig, samt regler för banan,
     exempelvis hur många trupper som måste komma igenom för att
     användaren ska vinna. Allt detta ska stå i XML-filen under varje
     bana.
**** DONE Vinst/förlust
     När användaren "vinner" en bana så skall spelet fråga användaren
     om den vill spela samma bana en gång till eller fortsätta till
     nästa bana.
**** DONE Restart level
     Den första menyn ska byggas ut med alternativet "restart level".
**** DONE Flera vägsträckor per bana
     Det ska finnas (möjlighet till, samt exempel på) flera möjliga
     vägsträckor per bana för trupperna.
**** DONE Spara resultat till Highscoreservice
     Alla resultat ska sparas med hjälp av det ni gjorde i
     lab 3. Highscores ska kunna visas (baserat på tid, poäng, nivå,
     etc.).
**** DONE Parameter som anger levels.xml vid start
     Ges ett argument vid körning ska detta tolkas som ett filnamn och
     läsas in istället för levels.xml och på så vis kan fler banor
     göras och köras utan att modifiera i spelets katalog.

*** TODO Nivå 3 (triggers / jar)
**** DONE Triggers, vägpekare i vägskäl?
     Banor ska kunna innehålla (och det ska finnas exempel på banor
     som innehåller) "växlar" som gör att en väg kan mynna ut i en
     T-korsning där spelaren klickar på ex. en knapp för att välja åt
     vilket håll trupperna ska gå.
**** DONE Skapa ett interface som har en metod "landOn".
**** DONE Banklasser implements landOn
     De olika områdena på en bana skall finnas i egna klasser. Varje
     sådan klass kan implementera interfacet beskrivet ovan.
**** DONE Läsa in class-filer från XML?
     Vid start av en ny bana så skall programmet läsa in de
     class-filer som XML-filen har deklarerat som sina områden och när
     truppmedlemmarna går på ett visst område så ska denna
     områdesklass funktion "landOn" anropas och någonting skall hända,
     förutsatt att den implementerat ovan nämnda interface.
**** TODO Områden, ex Teleporters
     Ett speciellt område utöver de beskrivna hittills skall
     finnas. Tex: Teleporters, när en truppmedlem går på en viss ruta
     så flyttas den till en annan på vägen.
**** TODO Teleportertrupper
     Teleportertrupper ska implementeras som kostar mer, men ska kunna
     lägga ner teleporterplattor när användaren väljer, och på så vis
     kommer trupperna att kunna slippa förflytta sig lika långt.
**** DONE Spelet sparas i AntiTD.jar 
     Spelet samt de filer som behövs för grundspelet skall finnas i en
     JAR-fil som heter "AntiTD.jar". Spelet ska startas med kommandot:
     java -jar AntiTD.jar

* Redovisning
  Den här laborationen har inte bara ett fungerande system som mål, utan
  den ska även vara en bas för vidareutveckling. Därför ska den visas
  upp för en fiktiv kund som ska köpa spelet av er för en stor hög
  pengar. Därför är det viktigt att ni gör bra ifrån er på både spelets
  uppvisning, och dokumentationen som ska lämnas in.
  
** Milestone DEADLINE: <2008-12-18 Thu 15:00>
   Innan jul ska ni boka in er för ett tillfälle hos handledarna för
   att redovisa projektets framskridande. Vid detta möte bör delar av
   programmet vara implementerat och ni bör kunna redovisa en plan (i
   form av följande dokument) för hur arbetet ska fortskrida fram till
   de olika redovisningstillfällena. Ni kommer att kunna boka in er
   för denna redovisning 18/12 mellan 15-17. Bokning av tid kommer att
   kunna ske senast veckan innan (info om detta kommer via mail). Om
   ni av någon anledning vet att ni ej kan delta vid detta tillfälle
   så hör av er till handledarna i förväg så kan redovisningen
   genomföras vid något tidigare tillfälle.
   
** Uppvisning (promotion) DEADLINE: <2009-01-13 Tue>
  Samtliga lösningar ska visas på gruppövningen den 13e
  januari. Ungefär 10 minuter per grupp ges. Tar uppvisningen för
  lång, eller för kort tid, minskar intresset från kunden. Hjälpmedel
  ni har att tillgå är: OH-apparat, ... Mer info kommer senare.
  
** Fysisk inlämning av dokumentation
   Det som i normala fall brukar vara en rapport om själva utvecklandet
   kommer på den här laborationen ha en annan uppgift.

   Utöver dokumentationen av programmet så ska ni även lämna in en
   detaljerad beskrivning av vem som gjort vad i projektet. Detta för att
   individuell poängbedömning ska kunna göras (var nogrann med
   denna). Denna del ska ges som en bilaga till rapporten. Till exempel
   kan en uppdaterad version av dokumentet från milestone tillfället
   ingpå. Observera att det inte räcker med en mening där ni säger att ni
   båda varit inblandade i allt och gjort lika mkt (lite mer detaljnivå
   krävs).

* Programdokumentation  DEADLINE: <2009-01-13 Tue 15:15>
  Dokumentation av arbetet ska vara inlämnad senast Tisdag 13/1-09
  15:15 Den största vikten i rapporten ligger på hur systemet är
  uppbyggt. Vilka delar ska modifieras för att olika effekter ska
  uppnås. Här hör ett eller flera UML-diagram hemma,
  algoritmbeskrivningar i grafisk form, etc.
  
  Tänk på att den här delen ska fungera som referenslitteratur. Det
  ska alltså inte vara uppsatser, utan listor, bilder, grafer,
  tabeller, träd, etc. Små textstycken behövs med, men en
  beskrivningar i uppsatsform är inget alternativ.

* Bedömning
  Rapporten kommer betygsättas utifrån dels utseende (teckensnittsval,
  marginalbredd, sidnumrering), innehåll (styckeuppdelning, förlopp, hur
  lätt det är att slå upp saker i den), informationsförmedling (vad
  berättas i rapporten), och slutligen kommer även stavfel,
  särskrivningar, meningsbyggnadsfel och språkblandningar in i bilden.

  JavaDoc-sidorna kommer gås igenom och deras innehåll kommer bedömas.

  Javakoden kommer synas så den följer konventioner och "bra"
  programmeringsstil. Ex. på mindre bra programmeringsstil är:
  
  : if (a.getP() < 4)
  :    return false;
  :  else
  :    return true;
  
  En helhetsbedömning av bl.a. ovanstående kommer poängsätta rapporten,
  kodens dokumentation, redovisning och källkoden. Resultatet kommer
  bli: Nivå Poäng 1 0 - 7 2 0 - 14 3 0 - 20
  
  Dokumentationen kommer stå för runt 1/3 av den totala poängen, så glöm
  inte att lägga ner tid på den.
  
  OBS: Poängsättningen av labben kommer att ske på den först inlämnade
  versionen. Fel som ger O på labben kommer givetvis att bidra till att
  minska poängtalet rätt rejält.
  
* Ev otydligheter i specifikationen
  Är något otydligt i specifikationen så är det upp till er att klargöra
  vad som gäller. Redovisa även sådana klargörande i rapporten.
