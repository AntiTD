package se.umu.cs.dit06ajnajs;

/**
 * Player is used to represent a player of the game.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class Player {
    
    private String name;
    private int credit;
    private int score;
    private int currentLevel;
    private int savedScore;
    

    /**
     * Creates a new unnamed <code>Player</code>, score, savedScore,
     * currentLevel is set to 0. An initial credit is given.
     */
    public Player() {
        this.name = "Unnamed player";
        this.score = 0;
        this.savedScore = 0;
        this.currentLevel = 0;
        initCredit();
    }

    /**
     * Sets the name of this player.
     *
     * @param name The name of this player.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of this player.
     *
     * @return The name of this player.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Adds credit to this player. Score is increased.
     *
     * @param credit The credit to add.
     */
    public void addCredit(int credit) {
        this.credit += credit;
        this.score += credit;
    }

    /**
     * Remove credit from this player.
     *
     * @param credit Amount of credit to remove.
     */
    public void removeCredit(int credit) {
        this.credit -= credit;
    }
    
    /** Sets an initial value of credits. */
    public void initCredit() {
        this.credit = 5000;
    }
    
    /**
     * Set the level number to play.
     *
     * @param num The level number to play.
     */
    public void setCurrentLevel(int num) {
        this.currentLevel = num;
    }
    
    /**
     * Save the current score to saved score.
     */
    public void saveScore() {
        this.savedScore = score;
    }
    
    /**
     * Resets the current score to the last saved score 
     */
    public void loadSavedScore() {
        this.score = this.savedScore;
    }
    
    /**
     * Return the saved score.
     *
     * @return The saved score.
     */
    public int getSavedScore() {
        return this.savedScore;
    }
    
    /**
     * Return players credit.
     *
     * @return Players credit.
     */
    public int getCredit() {
        return this.credit;
    }
    
    /**
     * Return the score of this player.
     *
     * @return The score of this player.
     */
    public int getScore() {
        return this.score;
    }
    
    /**
     * Return the level number this player is at.
     *
     * @return The level number this player is at.
     */
    public int getCurrentLevel() {
        return this.currentLevel;
    }
}
