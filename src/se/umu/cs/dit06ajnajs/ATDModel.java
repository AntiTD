package se.umu.cs.dit06ajnajs;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import se.umu.cs.dit06ajnajs.agent.AgentPrototypeFactory;
import se.umu.cs.dit06ajnajs.agent.Tower;
import se.umu.cs.dit06ajnajs.agent.Unit;
import se.umu.cs.dit06ajnajs.level.GoalSquare;
import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.level.StartSquare;

/**
 * ATDModel manages all instances of classes used in a game. Makes sure
 * everything is set correctly and provides methods to add and get instances to
 * the model.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jacobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class ATDModel { 
    private static Logger logger = Logger.getLogger("AntiTD");
    
    private Player player;
    private List<Unit> units;
    private List<Tower> towers;
    private List<Level> levels;
    private Level currentLevel;
    private Map<String, AudioClip> sounds;
    private List<URL> tracks;
    private int currentTrack;

    /**
     * Creates a new <code>ATDModel</code> instance. Initializes game sound and
     * sets currentLevel to the first one from the list supplied as a parameter.
     *
     * @param levels A list of levels to use.
     */
    public ATDModel(List<Level> levels) {
        this.levels = levels;
        this.currentLevel = levels.get(0);
        
        // Load sounds for model
        try {
            URL soundsURL = getClass().getResource("/resources/sounds/");
            sounds = new HashMap<String, AudioClip>();
            AudioClip sound = Applet.newAudioClip(new URL(soundsURL, "goal.wav"));
            sounds.put("goal", sound);
            sound = Applet.newAudioClip(new URL(soundsURL, "victory.wav"));
            sounds.put("victory", sound);

            // Music tracks
            tracks = new ArrayList<URL>();
            URL trackURL = new URL(soundsURL, "track1.mid");
            tracks.add(trackURL);
            trackURL = new URL(soundsURL, "track2.mid");
            tracks.add(trackURL);
            trackURL = new URL(soundsURL, "track3.mid");
            tracks.add(trackURL);
            trackURL = new URL(soundsURL, "track4.mid");
            tracks.add(trackURL);
            currentTrack = 0;
        } catch (IOException e) {
            System.err.println("Couldn't find sound. Exception: "
                               + e.getMessage());
        }
    }

    /**
     * Creates a new player and initialises the first level.
     */
    public void newGame() {
        this.player = new Player();
        initLevel();
        // TODO: ...reset stuff
    }
    
    /**
     * Restart the current level
     */
    public void restartLevel() {
        player.loadSavedScore();
        initLevel();
    }
    
    /**
     * Advance one level, changes the player and initializes the next level.
     *
     * @return true if level is advanced, false otherwise which means game is
     * won.
     */
    public boolean advanceLevel() {
        player.saveScore();
        if (player.getCurrentLevel() + 1 < levels.size()) {
            player.setCurrentLevel(player.getCurrentLevel() + 1);
            initLevel();
            return true;
        } else {
            // game won, ask for highscore entry
            return false;
        }
    }
    
    /**
     * Initialises the level that is next in turn for the player.
     */
    private void initLevel() {
        // Check if there is more levels
        if (player.getCurrentLevel() < levels.size()) {
            this.currentLevel = levels.get(player.getCurrentLevel());
            // If restartet game, the level must be reset
            this.currentLevel.resetLevel();
            this.towers = new ArrayList<Tower>();
            this.units = new ArrayList<Unit>();

            for (Tower tower : currentLevel.getTowers()) {
                towers.add(tower);
            }
            player.initCredit();
        } else {
            //TODO vad händer om det är slut på banor?
        }
    }
    
    /**
     * Adds a new Tower to the Level, it will be placed at a random free
     * TowerSquare.
     *
     * @param t The Tower to add.
     */
    public void addTower(Tower t) {
        currentLevel.addTower(t);
        towers.add(t);
    }

    /**
     * Adds a new Unit to the game if player can afford it. Cost will be
     * deducted from the players credits and the Unit will be added to the
     * currently active StartSquare.
     *
     * @param unit The Unit to add.
     * @return true if player can afford to but the Unit, false otherwise.
     */
    public boolean addUnit(Unit unit) {
        if (player.getCredit() >= unit.getCost()) {
            this.player.removeCredit(unit.getCost());
            StartSquare startSquare = this.currentLevel.getActiveStartSquare();
            unit.setLevel(this.currentLevel);
            startSquare.addUnit(unit);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Releases a Unit in the game, which means it will be added to the units
     * list and drawn on Level.
     *
     * @param unit The Unit to release.
     */
    public void releaseUnit(Unit unit) {
        units.add(unit);
    }

    /**
     * Returns a copy of the List containing all Units.
     *
     * @return A copy of the List containing all Units.
     */
    public List<Unit> getUnits() {
        // Copy array
        List<Unit> resultList = new ArrayList<Unit>(this.units.size());
        resultList.addAll(this.units);
        return resultList;
    }
    
    /**
     * Returns a copy of the List containing all Towers.
     *
     * @return A copy of the List containing all Towers.
     */
    public List<Tower> getTowers() {
        // Copy array
        List<Tower> resultList = new ArrayList<Tower>(this.towers.size());
        resultList.addAll(this.towers);
        return resultList;
    }

    /**
     * Removes all supplied units from this model. This is called when dead
     * units are cleared from a game.
     *
     * @param units A collection of units to remove.
     */
    public void removeUnits(Collection<Unit> units) {
        this.units.removeAll(units);
    }
    
    /**
     * Returns the current level.
     *
     * @return The current level.
     */
    public Level getCurrentLevel() {
        return this.currentLevel;
    }
    
    /**
     * Returns an Image of the Level.
     *
     * @return An Image of the Level.
     */
    public Image getMapImage() {
        return currentLevel.getMapImage();
    }
    
    /**
     * Returns the Dimension of the Level in pixels.
     *
     * @return The Dimension of the Level in pixels.
     */
    public Dimension getMapDimension() {
        return currentLevel.getDimension();    	
    }
    
    /**
     * Returns an array containing all GoalSquares.
     *
     * @return An array containing all GoalSquares.
     */
    public GoalSquare[] getGoalSquares() {
        return this.currentLevel.getGoalSquares();
    }

    /**
     * Returns an array containing all StartSquares.
     *
     * @return An array containing all StartSquares.
     */
    public StartSquare[] getStartSquares() {
        return this.currentLevel.getStartSquares();
    }

    /**
     * Adds credit to the player.
     *
     * @param credit The credit to be added
     */
    public void addCredit(int credit) {
        player.addCredit(credit);
    }

    /**
     * Returns the Credit of the Player.
     *
     * @return The Credit of the Player.
     */
    public int getCredit() {
        return player.getCredit();
    }
    
    /**
     * Returns the score from the player.
     *
     * @return The score from the player.
     */
    public int getScore() {
        return player.getScore();
    }
    
    /**
     * Returns the saved score from the Player.
     *
     * @return The saved score from the Player.
     */
    public int getSavedScore() {
        return this.player.getSavedScore();
    }
    
    /** Saves Player score. */
    public void saveScore() {
        player.saveScore();
    }
    
    /**
     * Returns the number of cleared Units on the current Level.
     *
     * @return The number of cleared Units on the current Level.
     */
    public int getNumOfClearedUnits() {
        return currentLevel.getNumOfClearedUnits();
    }
    
    /**
     * Return the number of Units required to win current Level.
     *
     * @return The number of Units required to win current Level.
     */
    public int getUnitsToWin() {
        return currentLevel.getUnitsToWin();
    }

    /**
     * Adds to the number of units that have reached the goalsquare
     * @param num Number of units that have reached the goalsquare
     */
    public void addGoalUnit(int num) {
        currentLevel.addClearedUnits(num);
    }
    
    /**
     * Checks if the goal for the current level is reached
     * @return True if the goal is reached
     */
    public boolean isLevelComplete() {
        return currentLevel.getNumOfClearedUnits() 
            >= currentLevel.getUnitsToWin();
    }
    
    /**
     * Checks if the level is lost
     * @return True if no units on the map and no credits to buy one unit
     */
    public boolean isLevelLost() {
        AgentPrototypeFactory factory = AgentPrototypeFactory.getInstance();
        int lowestCost = factory.getLowestCost();
        return (player.getCredit() <  lowestCost && units.size() == 0);
    }
    
    /**
     * Returns the sound matching the String
     * @param s The key for the sound
     * @return The sound as AudioClip
     */
    public AudioClip getSound(String s) {
        return sounds.get(s);
    }
    
    /**
     * Returns the track matching the String
     *
     * @return The sound as File.
     */
    public URL getNextTrack() {
        int nextIndex = (currentTrack + 1) % tracks.size();
        currentTrack = nextIndex;
        // TODO, verify. Should get next track from list.
        return tracks.get(currentTrack);
    }
}
