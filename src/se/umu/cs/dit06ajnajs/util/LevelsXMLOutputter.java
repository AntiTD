package se.umu.cs.dit06ajnajs.util;

import java.io.IOException;
import java.io.Writer;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.level.MapSquare;

/**
 * Converts a Level to XML.
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 * @version 1.0
 */
public final class LevelsXMLOutputter { 
    
    /**
     * Writes supplied Level to supplied Writer.
     *
     * @param level The Level to Write.
     * @param out The Writer to output to.
     */
    public static void outputMap(Level level, Writer out) {
        Document doc = makeDoc(level);
        try {
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            outputter.output(doc, out);
        } catch (IOException e) {
            System.err.println("Couldn't write file: " + e.getMessage());
        }
    }
    
    /**
     * Converts a Level to a Document.
     *
     * @param level The Level to convert.
     * @return The resulting Document.
     */
    private static Document makeDoc(Level level) {
        MapSquare[][] squareMatrix = level.toArray();        
        
        // Element rootElement = new Element(feed.getType());
        Element rootElement = new Element("levels");
        
        // Level
        Element levelElement = new Element("level");
        levelElement.setAttribute("name", level.getName());
        rootElement.addContent(levelElement);
        
        for (int r = 0; r < squareMatrix[0].length; r++) {
            Element row = new Element("row");
            for (int c = 0; c < squareMatrix.length; c++) {
                Element square = new Element("square");
                MapSquare mapSquare = squareMatrix[c][r];
                square.setAttribute("type",
                                    mapSquare.getClass().getSimpleName());
                row.addContent(square);
            }
            levelElement.addContent(row);
        }
        return new Document(rootElement);
    }
}