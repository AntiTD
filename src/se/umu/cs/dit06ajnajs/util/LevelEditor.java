package se.umu.cs.dit06ajnajs.util;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import se.umu.cs.dit06ajnajs.AntiTD;
import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.level.MapSquare;
import se.umu.cs.dit06ajnajs.level.MapSquarePrototypeFactory;
import se.umu.cs.dit06ajnajs.util.LevelsXMLOutputter;

/**
 * A level editor to make template XML-levels. All information needed to
 * make a complete level is not implemented.
 * 
 * TODO: unitsToWin, towers, save XML to user specified location.
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 * @version 1.0
 */
public class LevelEditor extends JFrame { 
    private static Logger logger = Logger.getLogger("AntiTD.levelEditor");
    
    private MapSquarePrototypeFactory factory =
        MapSquarePrototypeFactory.getInstance();
    private Level level;
    private JComponent mapComponent;
    private JList mapSquareList;
    private String selectedMapSquareType;
    

    /**
     * Initializes the editor. creates and shows a level containing only
     * TowerSquares
     */
    public LevelEditor() {
        // Size of the Level
        int cols = 16;
        int rows = 10;
        
        // Create a matrix containing TowerSquares
        MapSquare[][] mapMatrix = new MapSquare[cols][rows];
        for (int c = 0; c < cols; c++) {
            for (int r = 0; r < rows; r++) {
                mapMatrix[c][r] = factory.createMapSquare("TowerSquare",
                                                          c * AntiTD.SQUARE_SIZE,
                                                          r * AntiTD.SQUARE_SIZE);
            }
        }

        this.level = new Level("new level", mapMatrix);
        createAndShowGUI();
    }

    /**
     * Creates and show the GUI of this frame.
     */
    public void createAndShowGUI() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        this.mapComponent = new MapComponent();
        mapComponent.addMouseListener(new MapListener());
        mainPanel.add(mapComponent, BorderLayout.CENTER);
        
        JPanel controlsPanel = new JPanel(new BorderLayout());

        // JList mapSquares
        DefaultListModel mapSquaresListModel = new DefaultListModel();
        for (String type : factory.getMapSquareTypes()) {
            mapSquaresListModel.addElement(type);
        }
        
        this.mapSquareList = new JList(mapSquaresListModel);
        mapSquareList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mapSquareList.addListSelectionListener(new MapSquareListListener());
        
        controlsPanel.add(mapSquareList, BorderLayout.NORTH);
        
        // Buttons
        JButton saveButton = new JButton("Save Level");
        saveButton.setActionCommand("saveMap");
        saveButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    System.out.println("Dude");
                    try {
                        URL dirURL = this.getClass().getResource("/resources/");
                        // TODO: fix outFile path and name
                        File outFile = new File(dirURL.getPath(), "temp-levels.xml");
                        
                        Writer writer = new BufferedWriter(new FileWriter(outFile));

                        LevelsXMLOutputter.outputMap(level, writer);
                        System.out.println("Wrote file");
                    } catch (IOException e) {
                        // TODO - fix error message
                        e.printStackTrace();
                    }
                }
            });
        controlsPanel.add(saveButton, BorderLayout.SOUTH);
        
        // Main
        mainPanel.add(controlsPanel, BorderLayout.EAST);

        this.add(mainPanel);
        this.pack();
        this.setTitle("MapEditor");
        this.setVisible(true);
    }

    /**
     * Inner class that represent and displays the current Map.
     */
    private class MapComponent extends JComponent {
        public MapComponent() {
            super();
            this.setPreferredSize(level.getDimension());
        }
        
        @Override
        public void paintComponent(Graphics g) {
            logger.info("MapComponent paintComponent()");
            Image img = level.getMapImage();
            g.drawImage(img, 0, 0, null);
        }
    }

    /**
     * Listens on the Map and creates new MapSquares on clicked tiles.
     */
    private class MapListener extends MouseAdapter {
        @Override
        public void mouseReleased(MouseEvent me) {
            int x = me.getX();
            int y = me.getY();
            MapSquare square = level.getMapSquareAtPoint(x, y);
            logger.info("Mouse clicked @ (" +  x + ", " + y + ")");
            logger.info("MapSquare @ " + square);
            logger.info("Selected MapSquareType: " + selectedMapSquareType);
            MapSquare newSquare = factory.createMapSquare(selectedMapSquareType,
                                                          square.getX(),
                                                          square.getY());
            level.setMapSquareAtPoint(square.getX(), square.getY(), newSquare);
            mapComponent.repaint();
        }
    }

    /**
     * An inner class that handles changes in MapSquare list.
     */
    private class MapSquareListListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting() == false) {
                selectedMapSquareType = mapSquareList.getSelectedValue().toString();
            }
        }
    }

    /**
     * Starts the editor.
     *
     * @param args Is not used.
     */
    public static void main(String[] args) {
        new LevelEditor();
        Logger.getLogger("AntiTD.levelEditor").setLevel(java.util.logging.Level.INFO);
    }
}