package se.umu.cs.dit06ajnajs.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;

import se.umu.cs.dit06ajnajs.AntiTD;
import se.umu.cs.dit06ajnajs.agent.AgentPrototypeFactory;
import se.umu.cs.dit06ajnajs.agent.Direction;
import se.umu.cs.dit06ajnajs.agent.Tower;
import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.level.MapSquare;
import se.umu.cs.dit06ajnajs.level.MapSquareFactory;
import se.umu.cs.dit06ajnajs.level.StartSquare;

/**
 * LevelsXMLParser parses an XML file containing information about different
 * levels in AntiTD.
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 * @version 1.0
 */
public final class LevelsXMLParser {

    /**
     * Parses one URL which should point to a XML-file.
     *
     * @param url The URL to parse.
     * @return A List of newly created Maps for this Document.
     * @throws IOException If the URL isn't pointing to an accessable document.
     * @throws JDOMException If the URL isn't pointing to a parsable document.
     */
    public static List<Level> parse(URL url) throws IOException,
                                                    JDOMException {
        Document doc = XMLUtil.loadXml(url);
        return parse(doc);
    }

    /**
     * Parses one File which should point to a XML-file
     *
     * @param file The File to parse.
     * @return A List of newly created Maps for this Document.
     * @throws IOException If the File isn't pointing to an accessable document.
     * @throws JDOMException If the File isn't pointing to a parsable document.
     */
    public static List<Level> parse(File file) throws IOException,
                                                    JDOMException {
        Document doc = XMLUtil.loadXml(file);
        return parse(doc);
    }

    /**
     * Parses one Document.
     *
     * @param doc The Document to parse.
     * @return A List of newly created Maps for this Document.
     */
    private static List<Level> parse(Document doc) {
        List<Level> levels = new ArrayList<Level>();
        Element rootElement = doc.getRootElement();
        @SuppressWarnings("unchecked") // JDOM doesn't support Generics.
            List<Element> levelElements = rootElement.getChildren();
        for (Element levelElement : levelElements) {
            // Parse and add a map to levels
            levels.add(parseLevel(levelElement));
        }
        return levels;
    }

    /**
     * Parses one LevelElement.
     *
     * @param levelElement The level-element to parse.
     * @return A new Level for this level.
     */
    private static Level parseLevel(Element levelElement) {
        String name = levelElement.getAttributeValue("name");
        int unitsToWin = new Integer(levelElement.getAttributeValue("unitsToWin"));
        
        // <towers/>
        List<Tower> towers = new ArrayList<Tower>();
        AgentPrototypeFactory agentFactory = AgentPrototypeFactory.getInstance();
        
        @SuppressWarnings("unchecked") // JDOM doesn't support Generics.
        List<Element> towerElements = levelElement.getChild("towers").getChildren("tower");
        
        for (Element towerElement : towerElements) {
            String towerType = towerElement.getAttributeValue("type");
            Tower tower = agentFactory.createTower(towerType);
            towers.add(tower);            
        }
        
        // <rows/>
        List<List<MapSquare>> rows = new ArrayList<List<MapSquare>>();
        @SuppressWarnings("unchecked") // JDOM doesn't support Generics.
            List<Element> rowElements = levelElement.getChildren("row");
        // The current row
        int rowCount = 0;
        for (Element rowElement : rowElements) {
            // Parse and add a list of MapSquares to rows
            rows.add(parseRow(rowElement, rowCount));
            rowCount++;
        }
        
        // TODO: Nullpointer checks
        MapSquare[][] mapMatrix = new MapSquare[rows.get(0).size()][rows.size()];
        for (int y = 0; y < rows.size(); y++) {
            for (int x = 0; x < rows.get(0).size(); x++) {
                mapMatrix[x][y] = rows.get(y).get(x);
            }
        }
        return new Level(name, mapMatrix, towers, unitsToWin);
    }

    /**
     * Parses one row Element.
     *
     * @param rowElement The row-element to parse.
     * @param rowCount The row-number.
     * @return A List of newly created MapSquares.
     */
    private static List<MapSquare> parseRow(Element rowElement, int rowCount) {
        List<MapSquare> mapSquares = new ArrayList<MapSquare>();

        @SuppressWarnings("unchecked") // JDOM doesn't support Generics.
            List<Element> squareElements = rowElement.getChildren("square");
        // The column- square-position in row.
        int squareCount = 0;
        for (Element squareElement : squareElements) {
            mapSquares.add(parseSquares(squareElement, rowCount, squareCount));
            squareCount++;
        }
        return mapSquares;
    }

    /**
     * Parses one square-element, creates a new instance of a MapSquare.
     *
     * @param squareElement The Element to parse.
     * @param rowCount The row-number.
     * @param squareCount The square/column-number.
     * @return A new instance of a MapSquare.
     */
    private static MapSquare parseSquares(Element squareElement, int rowCount,
                                          int squareCount) {
        String type = squareElement.getAttributeValue("type");
        MapSquareFactory mapSquareFactory = MapSquareFactory.getInstance();
        // TODO: AntiTD.SQUARE_SIZE should be sent as a parameter, or set some
        // other way. SQUARE_SIZE set in XML means images also needs to be set
        // in XML.
        int x = squareCount * AntiTD.SQUARE_SIZE;
        int y = rowCount * AntiTD.SQUARE_SIZE;
        MapSquare resultSquare = mapSquareFactory.createMapSquare(type, x, y);
        if (type.equals("StartSquare")) {
            String directionString = squareElement.getAttributeValue("direction");
            if (directionString.equals("UP")) {
                ((StartSquare) resultSquare).setStartDirection(Direction.UP);
            } else if (directionString.equals("DOWN")) {
                ((StartSquare) resultSquare).setStartDirection(Direction.DOWN);
            } else if (directionString.equals("LEFT")) {
                ((StartSquare) resultSquare).setStartDirection(Direction.LEFT);
            } else if (directionString.equals("RIGHT")) {
                ((StartSquare) resultSquare).setStartDirection(Direction.RIGHT);
            } else {
                System.err.println("StartSquare doesn't contain a start Direction");
            }
        }
        return resultSquare;
    }
}
