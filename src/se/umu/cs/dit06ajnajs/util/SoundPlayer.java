package se.umu.cs.dit06ajnajs.util;

import java.applet.AudioClip;
import java.io.File;
import java.io.IOException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequencer;
import java.net.URL;

public final class SoundPlayer { 
    private static boolean mute = false;
    private static boolean musicRunning = true;
    private static Sequencer sequencer;
    private static Thread musicThread;


    public static boolean isMute() {
        return mute;
    }

    public static void setMute(boolean state) {
        mute = state;
        if (sequencer.isOpen()) {
            if (mute) {
                sequencer.stop(); 
            } else {
                sequencer.start();
            }
        }
    }

    /**
     * Play the supplied clip if mute is not set to true.
     *
     * @param clip The clip to play.
     */
    public static void play(final AudioClip clip) {
        new Thread(new Runnable() {
            public void run() {
                if (!mute) {
                    clip.play();
                }
            }
        }).start();
    }

    /**
     * Plays the provided Midi-file once
     *
     * @param midiFile The Midi-file
     */
//  TODO loop the midi
    public static void playMusic(final URL midiFile) {
        //musicRunning = true;
        // Play once
        musicThread = new Thread(new Runnable() {
            public void run() {
                try {
                    sequencer = MidiSystem.getSequencer();
                    sequencer.setSequence(MidiSystem.getSequence(midiFile));
                    sequencer.open();
                    sequencer.setLoopCount(sequencer.LOOP_CONTINUOUSLY);
                    if (!mute) {
                        sequencer.start();
                    }
                    
                    while(true) {
                        // Check if running
                        if(sequencer.isRunning()) {
                            Thread.sleep(1000); // Check every second
                        } else while (!sequencer.isRunning()) {
                            Thread.sleep(1000); // Check every second
                        }

                    }
                } catch (InterruptedException e) {

                } catch (MidiUnavailableException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InvalidMidiDataException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // This is the last thing that runs and then the thread is dead
                sequencer.stop();
                sequencer.close();
                //musicRunning = false;
            } 
        });
        if (true) {
            //System.out.println("Starting musicthread");
            musicThread.start();
        } else {
            System.out.println("Musicthread should not be started, musicRunning is false");
        }
        
    }

    public static void killMusic() {
        //musicRunning = false;
        if (musicThread != null) {
            System.out.println("Interupting musicThread...");
            musicThread.interrupt();
            musicRunning = true;
        }
    }
}

