package se.umu.cs.dit06ajnajs.util;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXParseException;
import org.xml.sax.SAXException;

//// Intended Usage //
//         XMLValidator errorHandler = new XMLValidator();
//         builder.setErrorHandler(errorHandler);

//         Document doc = builder.build(srcFile);
//         if (!errorHandler.isValid()) {
//             System.err.println("Supplied xml-file doesn't comply to schema");
//             System.err.println("Error: " + errorHandler.getException().getMessage());
//         } else if (!errorHandler.isFreeFromWarnings()) {            
//             System.err.println("Supplied xml-file doesn't comply to schema");
//             System.err.println("Warning: " + errorHandler.getException().getMessage());            
//         }

public class XMLValidator extends DefaultHandler {
    private boolean valid;
    private boolean freeFromWarnings;
    private SAXParseException exception;
        
    public XMLValidator() {
        this.valid = true;
        this.freeFromWarnings = true;
    }

    public boolean isValid() {
        return this.valid;
    }

    public boolean isFreeFromWarnings() {
        return this.freeFromWarnings;
    }

    public SAXParseException getException() {
        return this.exception;
    }
        
    @Override
    public void error(SAXParseException exception) throws SAXException {
        System.out.println("Error");
        this.valid = false;
        this.exception = exception;
    }
        
    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        System.out.println("Error");
        this.valid = false;
        this.exception = exception;
    }
        
    @Override
    public void warning(SAXParseException exception) throws SAXException {
        System.out.println("Warning");
        this.freeFromWarnings = false;
        this.exception = exception;
    }
}