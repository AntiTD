package se.umu.cs.dit06ajnajs.util;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * Class to load resources and return a validated org.jdom.Document.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @version 1.0
 */
public final class XMLUtil {

    /**
     * Loads an URL and returns a Document. See http://www.jdom.org/ for more
     * info.
     * @param srcURL An URL to a parsable source.
     * @return A newly created DOM Document from srcURL.
     * @throws IOException If the srcURL can't be found.
     * @throws JDOMException If the srcURL doesn't point to a resource which can
     * be converted to a Document.
     */
    public static Document loadXml(URL srcURL) throws IOException,
                                                      JDOMException {
        SAXBuilder builder = new SAXBuilder(true);
        builder.setFeature("http://apache.org/xml/features/validation/schema", true);
        Document doc = builder.build(srcURL);
        return doc;
    }

    /**
     * Loads an URL and returns a Document. See http://www.jdom.org/ for more
     * info.
     * @param srcFile A file to create a Document from.
     * @return A newly created DOM Document from srcFile.
     * @throws IOException If the srcURL can't be found.
     * @throws JDOMException If the srcURL doesn't point to a resource which can
     * be converted to a Document.
     */
    public static Document loadXml(File srcFile) throws IOException,
                                                        JDOMException {
        // New builder with validation set to true
        SAXBuilder builder = new SAXBuilder(true);
        builder.setFeature("http://apache.org/xml/features/validation/schema", true);
        Document doc = builder.build(srcFile);
        return doc;
    }
}
