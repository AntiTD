package se.umu.cs.dit06ajnajs.level;

/**
 * Is thrown if there is no active StartSquare
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 *         Andreas Jacobsson, dit06ajs@cs.umu.se
 * @version 1.0
 */
public class NoActiveStartSquareException extends RuntimeException { 
    public NoActiveStartSquareException() {
    }
    
    public NoActiveStartSquareException(String msg) {
        super(msg);
    }
}
