package se.umu.cs.dit06ajnajs.level;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;
import java.util.Set;

/**
 * MapSquarePrototypeFactory is used to create new instances of different
 * implementations of MapSquare from a String. Implemented as a Singleton.
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 * @version 1.0
 */
public class MapSquarePrototypeFactory {
    // private static Logger logger = Logger.getLogger("AntiTD");

    private static final MapSquarePrototypeFactory INSTANCE
        = new MapSquarePrototypeFactory();

    private java.util.Map<String, MapSquare> squareMap;

    /**
     * Gets the only instance of this class, if none exists one is created.
     *
     * @return The only instance of this class.
     */
    public static MapSquarePrototypeFactory getInstance() {
        return INSTANCE;
    }

    /**
     * MapSquarePrototypeFactory initialices all different types of MapSquares
     * and puts them in a java.util.Map.
     *
     * Private constructor makes sure there can only be one instance of this
     * class, and that instance is created by the class itself from the first
     * invocation of getInstance()
     *
     */
    private MapSquarePrototypeFactory() {
        squareMap = new HashMap<String, MapSquare>();

        //TODO read squaresettings from file

        try {
            // Get directory containing images
            // TODO: move to config class of file
            URL imagesURL = this.getClass().getResource("/resources/map-images/");

            // Create TowerSquare Prototype
            // URL url = this.getClass().getResource("/resources/grass.jpg");
            URL url = new URL(imagesURL, "TowerSquare.gif");
            BufferedImage image = ImageIO.read(url);
            squareMap.put("TowerSquare", new TowerSquare(-1, -1, image));

            // Create PathSquare Prototype
            url = new URL(imagesURL, "PathSquare.jpg");
            image = ImageIO.read(url);
            squareMap.put("PathSquare", new PathSquare(-1, -1, image));

            // Create GoalSquare Prototype
            url = new URL(imagesURL, "GoalSquare.gif");
            image = ImageIO.read(url);
            squareMap.put("GoalSquare", new GoalSquare(-1, -1, image));

            // Create StartSquare Prototype
            url = new URL(imagesURL, "StartSquare.gif");
            image = ImageIO.read(url);
            squareMap.put("StartSquare", new StartSquare(-1, -1, image));

            // Create BlockedSquare Prototype
            url = new URL(imagesURL, "BlockedSquare.gif");
            image = ImageIO.read(url);
            squareMap.put("BlockedSquare", new BlockedSquare(-1, -1, image));

            // Create TurSquare Prototype
            url = new URL(imagesURL, "TurnSquare.gif");
            image = ImageIO.read(url);
            // TODO: TurnSquare
            squareMap.put("TurnSquare", new TurnSquare(-1, -1, image));
        } catch (IOException e) {
            System.err.println("Couldn't find image. Exception: "
                               + e.getMessage());
        }
    }

    /**
     * Creates a new instance of a MapSquare with the type specified by the
     * first paramter.
     *
     * @param type A String representing a type exisiting in this factory.
     * @param x The x-position of the resulting MapSquare.
     * @param y  The y-position of the resulting MapSquare
     * @return A newly create MapSquare.
     */
    public MapSquare createMapSquare(String type, int x, int y) {
        MapSquare squarePrototype = squareMap.get(type);
        if (squarePrototype == null) {
            throw new IllegalArgumentException("The specified type '" + type
                                               + "' doesn't exist");
        }
        MapSquare resultSquare = (MapSquare) squarePrototype.clone();

        // Setup fields that should not be from original shallow clone
        resultSquare.init();
        resultSquare.setX(x);
        resultSquare.setY(y);
        return resultSquare;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("AgentPrototypeFactory is a "
                                             + "Singleton no clones are"
                                             + " supported.");
    }

    /**
     * Returns a Set containing all unitTypes this Class can produce.
     *
     * @return A Set containing all unitTypes this Class can produce.
     */
    public Set<String> getMapSquareTypes() {
        return squareMap.keySet();
    }
}