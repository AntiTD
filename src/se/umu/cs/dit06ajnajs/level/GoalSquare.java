package se.umu.cs.dit06ajnajs.level;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import se.umu.cs.dit06ajnajs.agent.Unit;

/**
 * Class GoalSquare extends PathSquare and implements Traversable. Contains 
 * functionality for collecting units that reaches GoalSquare
 * @author dit06ajs
 * @version 1.0
 */
public class GoalSquare extends PathSquare implements Traversable{
    private static Logger logger = Logger.getLogger("AntiTD");
    
    // TODO Eftersom denna töms efter varje tidssteg så behövs väl inte lista?
    private List<Unit> units;

    public GoalSquare(int x, int y, Image image) {
        super(x, y, image);
        units = new ArrayList<Unit>();
    }

    @Override
    public void landOn(Unit unit) {
        logger.info("Unit >" + unit.getClass().getSimpleName() + 
        "< landed on goalsquare. Unit will be removed next timestep.");
        units.add(unit);
        unit.setAlive(false);
    }
    
    /**
     * Calculates the credit and clears the units 
     * @return The credit
     */
    public int getCredit() {
        int credit = units.size() * 100;
        units.clear();
        return credit;
    }

    
    /**
     * Returns number of units in square.
     *
     * @return Number of units in square.
     */
    public int getNumUnits() {
        return units.size();
    }
}
