package se.umu.cs.dit06ajnajs.level;

import java.awt.Image;
import se.umu.cs.dit06ajnajs.agent.Unit;
import java.util.logging.Logger;
import se.umu.cs.dit06ajnajs.agent.Direction;
import java.awt.Graphics;
import java.awt.Color;
import se.umu.cs.dit06ajnajs.AntiTD;
import java.util.Observer;
import java.util.Observable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class StartSquare extends MapSquare and implements Traversable and Observer.
 * A StartSquare can recive units that are about to be released. It has 
 * functionality to release units without causing collisions
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @version 1.0
 */
public class StartSquare extends MapSquare implements Traversable, Observer {
    private static Logger logger = Logger.getLogger("AntiTD");
    private boolean active;
    private List<Unit> unitsToStart;
    

    private Direction startDirection =  Direction.UP;
    
    /**
     * Creates a new StartSquare, calls extended MapSquares contructor with
     * supplied parameters. Sets this StartSquare as not active.
     * 
     * @param x The x-coordinate of this Square.
     * @param y The y-coordinate of this Square.
     * @param image The Image used to display this Square.
     */
    public StartSquare(int x, int y, Image image) {
        super(x, y, image);
        this.active = false;
    }
    
    /**
     * Used to initialize fields that should be individually set for every
     * StartSquare, since clone() is shallow.
     */
    @Override
    public void init() {
        this.unitsToStart = new ArrayList<Unit>();
    }

    /**
     * Returns true if this StartSquare is active, false otherwise.
     *
     * @return True if this StartSquare is active, false otherwise.
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the start Direction of this MapSquare.
     *
     * @param directioin The start Direction of this MapSquare.
     */
    public void setStartDirection(Direction directioin) {
        this.startDirection = directioin;
    }

    /**
     * Returns the start Direction of this StartSquare.
     *
     * @return The start Direction of this StartSquare.
     */
    public Direction getStartDirection() {
        return this.startDirection;
    }

    /**
     * Returns the Unit in turn to start from this StartSquare.
     *
     * @return The Unit in turn to start from this StartSquare. If no Unit is
     * about to start null is returned.
     */
    public Unit getUnitToStart() {
        if (!unitsToStart.isEmpty()) {
            return unitsToStart.get(0);
        } else {
            return null;
        }
    }

    /**
     * Adds a Unit to this StartSquare, sets its position to match the center
     * point of this square, and sets the Units Direction to the Direction of
     * this StartSquare.
     *
     * @param unit The Unit to add.
     */
    public void addUnit(Unit unit) {
        unitsToStart.add(unit);
        unit.setCenterX(getCenterX());        
        unit.setCenterY(getCenterY());
        unit.setDirection(getStartDirection(), this);
    }

    /**
     * Removes specified Unit from the start queue.
     *
     * @param unit The Unit to remove.
     */
    public void removeUnit(Unit unit) {
        unitsToStart.remove(unit);
    }

    /**
     * Sets the landing Units Direction to the Direction of this StartSquare.
     *
     * @param unit The Unit that land on this square.
     */
    public void landOn(Unit unit) {
        unit.setDirection(startDirection, this);
    }

    /**
     * Sets this StartSquare as active.
     */
    @Override
    public void click() {
        if (!active) {
            setChanged();
            notifyObservers();
            clearChanged();
            this.active = true;
        }
    }
    
    /**
     * Paints this square on the specified Graphics objects. Calls
     * super.paint(p) and paints a green bar at bottom of this square if the
     * StartSquare is active.
     *
     * @param g The Graphics object to paint to.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        
        if (active) {
            g.setColor(Color.GREEN);
            g.fillRect(getX(), getY() + AntiTD.SQUARE_SIZE -2, AntiTD.SQUARE_SIZE, 2);
        }
    }

    /**
     * Called when another StartSquare is activated, which means that this
     * StartSquare should not be active.
     *
     * @param observer an <code>Observable</code> value
     * @param obj an <code>Object</code> value
     */
    public void update(Observable observer, Object obj) {
        logger.info("Update in StartSquare: active is set to false");
        this.active = false;
    }
}