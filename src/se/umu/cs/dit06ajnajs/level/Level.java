package se.umu.cs.dit06ajnajs.level;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import se.umu.cs.dit06ajnajs.AntiTD;
import se.umu.cs.dit06ajnajs.agent.Direction;
import java.util.Collection;
import se.umu.cs.dit06ajnajs.agent.Tower;

/**
 * Level is used to contain and manage all information about a level beeing
 * played in the game.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jacobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class Level {
    private static Logger logger = Logger.getLogger("AntiTD");
    
    private String name;
    private int unitsToWin;
    private int clearedUnits;
    private int squareSize;
    private int width;
    private int height;
    private int numCol;
    private int numRow;
    private MapSquare[][] squareMatrix;
    private GoalSquare[] goalSquares;
    private StartSquare[] startSquares;
    private Collection<Tower> towers;
    
    /**
     * Sets parameters and adds Towers to random locations.
     *
     * @param name The name of this Level.
     * @param squareMatrix A matrix containing MapSquares. 
     * @param towers Towers to add to this Level.
     * @param unitsToWin The amount of Units needed in GoalSquares to win this
     * Level.
     */
    public Level(String name, MapSquare[][] squareMatrix,
                 Collection<Tower> towers, int unitsToWin) {
        this(name, squareMatrix);
        
        // Add towers to TowerSquares
        this.towers = towers;
        this.unitsToWin = unitsToWin;
        for (Tower tower : towers) {
            addTower(tower);
        }
    }
    
    /**
     * Calculates and sets variables such as width and height based on the
     * supplied matrix containing MapSquares. Makes every StartSquare observe
     * alle the other. Initializes all TurnSquare to set all valid Direction.
     *
     * @param name The name of this Level.
     * @param squareMatrix A matrix containing MapSquares. 
     */
    public Level(String name, MapSquare[][] squareMatrix) {
        this.name = name;
        this.squareSize = AntiTD.SQUARE_SIZE;
        this.squareMatrix = squareMatrix;
        this.clearedUnits = 0;
        try {
            this.numCol = squareMatrix.length;
            this.numRow = squareMatrix[0].length;
        } catch(NullPointerException e) {
            e.printStackTrace();
            return;
        }

        // Set width and height for map
        this.width = squareSize * numCol;
        this.height = squareSize * numRow;

        this.goalSquares = extractGoalSquares();
        this.startSquares = extractStartSquares();
        // Make all startSquares observe each other
        for (StartSquare s1 : startSquares) {
            for (StartSquare s2 : startSquares) {
                if (s1 != s2) {
                    logger.info("StartSquare observable: " + s1
                                + ", is observed by " + s2);
                    s2.addObserver(s1);
                }
            }
        }
        initTurnSquares();
    }
    
    /**
     * Reset the Level information, clears all Units, activates on StartSquare,
     * reset Towers.
     */
    public void resetLevel() {
        this.clearedUnits = 0;
        // Set one StartSquare as active
        startSquares[0].click();
        this.resetTowers();
        this.resetStartSquares();
    }

    /**
     * Cleares queued units from start squares
     * @author dit06ajs
     */
    private void resetStartSquares() {
        for (StartSquare square : this.startSquares) {
            square.init();
        }
    }

    /**
     * Returns the MapSquare at given x- y-coordinate.
     *
     * @param x The x-value to check.
     * @param y The y-value to check.
     * @return The MapSquare at given x- y-coordinate or null if none exist.
     */
    public MapSquare getMapSquareAtPoint(int x, int y) {
        return getMapSquareAtPoint(new Point(x, y));    	
    }

    /**
     * Returns the MapSquare at given x- y-coordinate.
     *
     * @param point The point to check.
     * @return The MapSquare at given x- y-coordinate or null if none exist.
     */
    public MapSquare getMapSquareAtPoint(Point point) {
        //TODO testa algoritmen
        int x = point.x;
        int y = point.y;

        if (x > width || y > height
            || x < 0 || y < 0) {
            // Out of bounds
            return null;
        }

        int col = x / AntiTD.SQUARE_SIZE;
        int row = y / AntiTD.SQUARE_SIZE;

        return squareMatrix[col][row];
    }
    
    /**
     * Sets a new MapSquare at given x- y-coordinate.
     *
     * @param x The x-coordinate.
     * @param y The y-coordinate.
     * @param mapSquare The MapSquare to set at given x- y-coordinates .
     */
    public void setMapSquareAtPoint(int x, int y, MapSquare mapSquare) {
        setMapSquareAtPoint(new Point(x, y), mapSquare);
    }
    
    /**
     * Sets a new MapSquare at given x- y-coordinate.
     *
     * @param point The point to set a new MapSquare.
     * @param mapSquare The MapSquare to set at given point.
     */
    public void setMapSquareAtPoint(Point point, MapSquare mapSquare) {
        int x = point.x;
        int y = point.y;

        if (x > width || y > height) {
            throw new IllegalArgumentException("Position is " +
            "outside of map bounds.");
        }

        int col = x / AntiTD.SQUARE_SIZE;
        int row = y / AntiTD.SQUARE_SIZE;
        
        squareMatrix[col][row] = mapSquare;
    }

    /**
     * Creates a new Image representing this Level and returns it.
     *
     * @return A new Image representing this Level and returns it.
     */
    public Image getMapImage() {
        logger.fine("------> getMapImage() -> " + Thread.currentThread().toString());
        Image backgroundImage = new BufferedImage(width, height,
                                                  BufferedImage.TYPE_INT_RGB);
        Graphics g = backgroundImage.getGraphics();
        
        for (MapSquare[] row : squareMatrix) {
            for (MapSquare square : row) {
                square.paint(g);
            }
        }
        return backgroundImage;
    }

    /**
     * Returns the Dimension of this Level.
     *
     * @return The Dimension of this Level.
     */
    public Dimension getDimension() {
        return new Dimension(width, height);
    }

    /**
     * Extract and return all GoalSquares in this Level.
     *
     * @return All GoalSquares in this Level.
     */
    private GoalSquare[] extractGoalSquares() {
        List<GoalSquare> squares = new ArrayList<GoalSquare>();
        for (MapSquare[] row : squareMatrix) {
            for (MapSquare square : row) {
                if (square instanceof GoalSquare) {
                    squares.add((GoalSquare) square);
                }
            }
        }
        GoalSquare[] goalSquares
            = squares.toArray(new GoalSquare[squares.size()]);
        return goalSquares;
    }
    
    /**
     * Extract all StartSquares from this Level.
     *
     * @return All StartSquares in this Level.
     */
    private StartSquare[] extractStartSquares() {
        List<StartSquare> squares = new ArrayList<StartSquare>();
        for (MapSquare[] row : squareMatrix) {
            for (MapSquare square : row) {
                if (square instanceof StartSquare) {
                    squares.add((StartSquare) square);
                }
            }
        }
        StartSquare[] startSquares
            = squares.toArray(new StartSquare[squares.size()]);
        return startSquares;
    }

    /**
     * Returns a random free TowerSquare.
     *
     * @return A random free TowerSquare.
     */
    private TowerSquare getRandomFreeTowerSquare() {
        List<TowerSquare> squares = extractTowerSquares();
        List<TowerSquare> freeSquares = new ArrayList<TowerSquare>();

        for (TowerSquare square : squares) {
            if (square.isAvailable()) {
                freeSquares.add(square);
            }
        }
        if (freeSquares.isEmpty()) {
            // TODO What should happen if there are no free towersquares?
            return null;
        }
        int index = (int) (freeSquares.size()*Math.random());
        return freeSquares.get(index);
    }
    
    public List<TowerSquare> extractTowerSquares() {
        // TODO What should happen if there are no towersquares?
        List<TowerSquare> squares = new ArrayList<TowerSquare>();
        for (MapSquare[] row : squareMatrix) {
            for (MapSquare square : row) {
                if (square instanceof TowerSquare) {
                    squares.add((TowerSquare) square);
                }
            }
        }
        return squares;
    }

    /**
     * Extract all MapSquare of type TurnSquare from this Level.
     *
     * @return All TurnSquares in this Level.
     */
    public List<TurnSquare> extractTurnSquares() {
        // TODO What should happen if there are no TurnSquares?
        List<TurnSquare> squares = new ArrayList<TurnSquare>();
        for (MapSquare[] row : squareMatrix) {
            for (MapSquare square : row) {
                if (square instanceof TurnSquare) {
                    squares.add((TurnSquare) square);
                }
            }
        }
        return squares;
    }

    /**
     * Returns every neighbouring MapSquare of the supplied MapSquare within
     * given range.
     * 
     * @param square The square to get neighbours from.
     * @param range Should be an int describing how many MapSquares away from
     * specified MapSquares to return.
     * @return A list of neighbouring MapSquares.
     */
    public List<MapSquare> getNeighbours(MapSquare square, int range) {
        List<MapSquare> neighbours = new ArrayList<MapSquare>();
        
        // Calculate row and col
        int col = square.getX() / AntiTD.SQUARE_SIZE;
        int row = square.getY() / AntiTD.SQUARE_SIZE;

        // Get width to scan for neighbours
        int scanWidth = range * 2 + 1;

        // Get top left position to start scanning of neighbours 
        int colLeft = col - range;
        int rowTop = row - range;
        
        logger.info("Range: " + range + ", colLeft: " + colLeft
                    + ", rowTop: " + rowTop + ", scanWidth: " + scanWidth);
        
        // Nestled loop from top-left position
        for (int tmpRow = rowTop;
             tmpRow < scanWidth + rowTop;
             tmpRow++) {
            for (int tmpCol = colLeft;
                 tmpCol < scanWidth + colLeft;
                 tmpCol++) {
                if (tmpCol >= 0 && tmpCol < numCol // Col is inside bounds
                    // Row is inside bounds
                    && tmpRow >= 0 && tmpRow < numRow 
                    // Col Row should not point to square to find neighbours
                    // from
                    && (tmpRow != row || tmpCol != col)) {
                    // Neighbour found
                    logger.info("Adding neigbour for: " + square + "\n"
                                + " At col: " + tmpCol + ", row: " + tmpRow);
                    neighbours.add(squareMatrix[tmpCol][tmpRow]);
                }
            }
        }
        logger.info(neighbours.size() + " neighbours found.");
        return neighbours;
    }
    
    /**
     * Calculate and set every possible direction the TurnSquares in this Level
     * can have. Adds a Direction to every neighbouring MapSquare that is a
     * TurnSquare.
     */
    private void initTurnSquares() {
        for (TurnSquare turnSquare : extractTurnSquares()) {
            int col = turnSquare.getX() / AntiTD.SQUARE_SIZE;
            int row = turnSquare.getY() / AntiTD.SQUARE_SIZE;
            // UP
            if (row - 1 >= 0
                && squareMatrix[col][row - 1] instanceof PathSquare) {
                turnSquare.addDirection(Direction.UP);
            }
            // RIGHT
            if (col + 1 < numCol
                && squareMatrix[col + 1][row] instanceof PathSquare) {
                turnSquare.addDirection(Direction.RIGHT);
            }
            // DOWN
            if (row + 1 < numRow
                && squareMatrix[col][row + 1] instanceof PathSquare) {
                turnSquare.addDirection(Direction.DOWN);
            }
            // LEFT
            if (col - 1 >= 0
                && squareMatrix[col - 1][row] instanceof PathSquare) {
                turnSquare.addDirection(Direction.LEFT);
            }
        }
    }
    
    /**
     * Return every GoalSquare in this Level.
     *
     * @return every GoalSquare in this Level.
     */
    public GoalSquare[] getGoalSquares() {
        return this.goalSquares;
    }

    /**
     * Return every StartSquare in this Level.
     *
     * @return every StartSquare in this Level.
     */
    public StartSquare[] getStartSquares() {
        return this.startSquares;
    }

    /**
     * Gets the name of this Level.
     *
     * @return The name of this Level.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns an Array representation of this map.
     *
     * @return An Array representation of this map.
     */
    public MapSquare[][] toArray() {
        return squareMatrix;
    }

    /**
     * Return the active StartSquare in this Level.
     *
     * @return The active StartSquare in this Level.
     */
    public StartSquare getActiveStartSquare() {
        for (StartSquare square : startSquares) {
            if (square.isActive()) {
                return square;
            }
        }
        throw new NoActiveStartSquareException();
    }

    /**
     * Add a Tower to this Level. Will place the Tower at a random
     * TowerSquare. Every neighbouring PathSquare is set to observe the Tower.
     *
     * @param t The Tower to add.
     */
    public void addTower(Tower t) {
        TowerSquare square = getRandomFreeTowerSquare();
        if (square != null) {
            Point tPos = new Point(square.getCenterX() - (t.getWidth()/2)
                    , square.getCenterY() - (t.getHeight()/2));
            t.setPostition(tPos);
            square.setTower(t);
            
            logger.info("Tower placed @ (" + tPos.x + ", " + tPos.y + ")");
            
            //Register as observer for the neighbours in range
            int shootRange = t.getRange();
            int squareRange = 
                (int) Math.ceil((shootRange - AntiTD.SQUARE_SIZE*0.5) 
                        / (AntiTD.SQUARE_SIZE)); 
            
            List<MapSquare> neighbours 
                = getNeighbours(square, squareRange);
            logger.info("There are >" + neighbours.size() 
                    + "< neighbours in range >" + t.getRange() + "< pixels");
            
            for (MapSquare neighbour: neighbours) {
                if (neighbour instanceof PathSquare) {
                    logger.info("Adding tower-observer: " + t
                                + " to: " + neighbour);
                    neighbour.addObserver(t);                    
                }
            }
        } else {
            throw new IllegalArgumentException("No available towersquares");
        }
    }

    /**
     * Returns all Towers in this Level.
     *
     * @return All Towers in this Level.
     */
    public Collection<Tower> getTowers() {
        return this.towers;
    }
    
    /**
     * Resets the towers.
     */
    public void resetTowers() {
        for (Tower tower : this.towers) {
            tower.init();
        }
    }
    
    /**
     * Returns the number of Units needed in goal to win this Level.
     *
     * @return The number of Units needed in goal to win this Level.
     */
    public int getUnitsToWin() {
        return this.unitsToWin;
    }
    
    /**
     * Returns the number of units that have reached a goalsquare.
     * @return The number of units that have reached a goalsquare.
     */
    public int getNumOfClearedUnits() {
        return this.clearedUnits;
    }
    
    /**
     * Adds to the number of units that have reached a goalsquare.
     * @param num Number of new units that have reached a goalsquare.
     */
    public void addClearedUnits(int num) {
        this.clearedUnits += num;
    }
}
