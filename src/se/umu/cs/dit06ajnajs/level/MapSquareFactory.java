package se.umu.cs.dit06ajnajs.level;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;
import java.util.Set;
import java.awt.Image;

/**
 * MapSquareFactory is used to create new instances of different
 * implementations of MapSquare from a String. Implemented as a Singleton.
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 * @version 1.0
 */
public class MapSquareFactory {
    // private static Logger logger = Logger.getLogger("AntiTD");

    private static final MapSquareFactory INSTANCE
        = new MapSquareFactory();

    private java.util.Map<String, Image> imageMap;

    /**
     * Gets the only instance of this class, if none exists one is created.
     *
     * @return The only instance of this class.
     */
    public static MapSquareFactory getInstance() {
        return INSTANCE;
    }

    /**
     * MapSquareFactory initialices all different types of MapSquares
     * and puts them in a java.util.Map.
     *
     * Private constructor makes sure there can only be one instance of this
     * class, and that instance is created by the class itself from the first
     * invocation of getInstance()
     *
     */
    private MapSquareFactory() {
        imageMap = new HashMap<String, Image>();

        //TODO read squaresettings from file

        try {
            // Get directory containing images
            // TODO: move to config class of file
            URL imagesURL = this.getClass().getResource("/resources/map-images/");
            URL url;
            BufferedImage image;
            
            // Create TowerSquare 
            // URL url = this.getClass().getResource("/resources/grass.jpg");
            url = new URL(imagesURL, "TowerSquare.gif");
            image = ImageIO.read(url);
            imageMap.put("TowerSquare", image);

            // Create PathSquare 
            url = new URL(imagesURL, "PathSquare.jpg");
            image = ImageIO.read(url);
            imageMap.put("PathSquare", image);

            // Create GoalSquare 
            url = new URL(imagesURL, "GoalSquare.gif");
            image = ImageIO.read(url);
            imageMap.put("GoalSquare", image);


            // Create StartSquare
            url = new URL(imagesURL, "StartSquare.gif");
            image = ImageIO.read(url);
            imageMap.put("StartSquare", image);

            // Create BlockedSquare 
            url = new URL(imagesURL, "BlockedSquare.gif");
            image = ImageIO.read(url);
            imageMap.put("BlockedSquare", image);

            // Create TurnSquare
            url = new URL(imagesURL, "TurnSquare.gif");
            image = ImageIO.read(url);
            // TODO: TurnSquare
            imageMap.put("TurnSquare", image);
        } catch (IOException e) {
            System.err.println("Couldn't find image. Exception: "
                               + e.getMessage());
        }
    }

    /**
     * Creates a new instance of a MapSquare with the type specified by the
     * first paramter.
     *
     * @param type A String representing a type exisiting in this factory.
     * @param x The x-position of the resulting MapSquare.
     * @param y  The y-position of the resulting MapSquare
     * @return A newly create MapSquare.
     */
    public MapSquare createMapSquare(String type, int x, int y) {
        Image img = imageMap.get(type);
        if (img == null) {
            throw new IllegalArgumentException("The specified type '" + type
                                               + "' doesn't exist");
        }
        MapSquare resultSquare = null;
        if (type.equals("BlockedSquare")) {
            resultSquare = new BlockedSquare(x, y, img);
        } else if (type.equals("GoalSquare")) {
            resultSquare = new GoalSquare(x, y, img);
        } else if (type.equals("PathSquare")) {
            resultSquare = new PathSquare(x, y, img);
        } else if (type.equals("StartSquare")) {
            resultSquare = new StartSquare(x, y, img);
        } else if (type.equals("TowerSquare")) {
            resultSquare = new TowerSquare(x, y, img);
        } else if (type.equals("TurnSquare")) {
            resultSquare = new TurnSquare(x, y, img);
        } else {
            throw new IllegalArgumentException("The specified type '" + type
                                               + "' doesn't exist");
        }
        // TODO, from cloneing in previous implementation
        resultSquare.init();
        return resultSquare;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("AgentFactory is a "
                                             + "Singleton no clones are"
                                             + " supported.");
    }

    /**
     * Returns a Set containing all unitTypes this Class can produce.
     *
     * @return A Set containing all unitTypes this Class can produce.
     */
    public Set<String> getMapSquareTypes() {
        return imageMap.keySet();
    }
}