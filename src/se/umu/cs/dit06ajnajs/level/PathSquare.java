package se.umu.cs.dit06ajnajs.level;

import java.awt.Image;
import java.util.logging.Logger;

import se.umu.cs.dit06ajnajs.agent.Unit;

/**
 * Class PathSquare extends MapSquare and implements Traversable.
 * Notifies observers when Unit calls on method landOn
 * @author dit06ajs
 * @author dit06ajn
 * @version 1.0
 */

public class PathSquare extends MapSquare implements Traversable {
    private static Logger logger = Logger.getLogger("AntiTD");
    
    /**
     * @param x The x-coordinate of this Square.
     * @param y The y-coordinate of this Square.
     * @param img The Image used to display this Square.
     */
    public PathSquare(int x, int y, Image img) {
        super(x, y, img);
    }

    /**
     * Notifies registered observers with reference to calling Unit
     */
    public void landOn(Unit unit) {
        // DONE: Woha a unit is here, I should tell all my towers!
        logger.info("Unit >" + unit.getClass().getSimpleName() + 
                    "< landed on pathsquare. Notifying observers...");
        setChanged();
        notifyObservers(unit);
    }
}