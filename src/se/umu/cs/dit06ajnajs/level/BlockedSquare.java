package se.umu.cs.dit06ajnajs.level;

import java.awt.Image;

/**
 * Class BlockedSquare extends MapSquare. Acts as a blocked square without
 * function 
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @version 1.0
 */
public class BlockedSquare extends MapSquare {

    /**
     * Creates a new <code>BlockedSquare</code> instance.
     *
     * @param x The x-coordinate of this Square.
     * @param y The y-coordinate of this Square.
     * @param image The Image used to display this Square.
     */
    public BlockedSquare(int x, int y, Image image) {
        super(x, y, image);
    }

}
