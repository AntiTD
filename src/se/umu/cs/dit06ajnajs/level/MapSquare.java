package se.umu.cs.dit06ajnajs.level;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.Observable;

import se.umu.cs.dit06ajnajs.Clickable;
import se.umu.cs.dit06ajnajs.Paintable;
import se.umu.cs.dit06ajnajs.AntiTD;
import java.awt.Rectangle;

/**
 * Abstract class MapSquare represents a square with a graphical representation
 * and a position.
 *  
 * @author dit06ajs
 * @author dit06ajn
 * @version 1.0
 */
public abstract class MapSquare extends Observable implements Paintable,
                                                              Cloneable,
                                                              Clickable {
    private int x;
    private int y;
    private int centerX;
    private int centerY;

    private Image image;

    /**
     * Creates new instans with position and image
     * @param x the x coordinate of the square
     * @param y the y coordinate of the square
     * @param image the image of the square
     */
    public MapSquare(int x, int y, Image image) {
        setX(x);
        setY(y);
        this.image = image;
    }

    /**
     * Used to initialize fields that should be individually set for every
     * MapSquare, since clone() is shallow.
     *
     * Empty method, ovveridden by subclass to implement behaviour.
     */
    public void init() {
        
    }

    /**
     * Paints this MapSquares Image to the specified Grahics object.
     *
     * @param g The Graphics to paint to.
     */
    public void paint(Graphics g) {
        g.drawImage(image, x, y, null);
    }

    /**
     * Returns the Images used by this MapSquare.
     *
     * @return The Images used by this MapSquare.
     */
    public Image getImage() {
        return this.image;
    }
    
    /**
     * Returns the top left point of this MapSquare.
     *
     * @return The top left point of this MapSquare.
     */
    public Point getPosition() {
        Point p = new Point(x, y);
        return p;
    }

    /**
     * Returns the center position of this MapSquare.
     *
     * @return The center position of this MapSquare.
     */
    public Point getCenterPoint() {
        return new Point(this.centerX, this.centerY);
    }

    /**
     * Returns the center x position of this MapSquare.
     *
     * @return The center x position of this MapSquare.
     */
    public int getCenterX() {
        return this.centerX;
    }
    
    /**
     * Returns the center y position of this MapSquare.
     *
     * @return The center y position of this MapSquare.
     */
    public int getCenterY() {
        return this.centerY;
    }
    
    /**
     * Returns the x position of this MapSquare.
     *
     * @return The x position of this MapSquare.
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the x position and recalculates center x position of this MapSquare.
     *
     * @param x The new x center position.
     */
    public void setX(int x) {
        this.x = x;
        this.centerX = this.x + (int) Math.round(AntiTD.SQUARE_SIZE/2.0);
    }

    /**
     * Returns the center y position of this MapSquare.
     *
     * @return The center y position of this MapSquare.
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the y position and recalculates center x position of this MapSquare.
     *
     * @param y The new y center position.
     */
    public void setY(int y) {
        this.y = y;
        this.centerY = this.y + (int) Math.round(AntiTD.SQUARE_SIZE/2.0);
    }

    /**
     * Sets the Image of this MapSquare.
     *
     * @param img The Image of this MapSquare.
     */
    public void setImage(Image img) {
        image = img;
    }

    /**
     * Empty method, ovveridden by subclass to implement behaviour when a user
     * clicks this MapSquare.
     */
    public void click() {
    }

    /**
     * Checks if the specified x and y position is inside of bounds.
     *
     * @param x The x-value to check.
     * @param y The y-value to check.
     * @return If the specified x and y position is inside of bounds.
     */
    public boolean contains(int x, int y) {
        return new Rectangle(this.x, this.y,
                             AntiTD.SQUARE_SIZE, AntiTD.SQUARE_SIZE).contains(x, y);
    }

    /**
     * Attemts to clone this MapSquare.
     *
     * @return A new instance of the same type as the instantiated MapSquare.
     */
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new Error("Object " + this.getClass().getName()
                    + " is not Cloneable");
        }
    }

    @Override
    public String toString() {
        return "MapSquare: " + this.getClass().getName()
        + " @("+ x +", " + y +")";
    }
}