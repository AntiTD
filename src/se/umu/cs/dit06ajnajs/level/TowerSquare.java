package se.umu.cs.dit06ajnajs.level;

import java.awt.Image;
import se.umu.cs.dit06ajnajs.agent.Tower;

/**
 * A MapSquare that can be used by Towers.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jacobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class TowerSquare extends MapSquare {
    
    private Tower tower;
    private boolean available;
    
    /**
     * Creates a new TowerSquare, calls extended MapSquares contructor with
     * supplied parameters. Sets this StartSquare as available for Towers.
     * 
     * @param x The x-coordinate of this Square.
     * @param y The y-coordinate of this Square.
     * @param img The Image used to display this Square.
     */
    public TowerSquare(int x, int y, Image img) {
            super(x, y, img);
            this.available = true;
        }
    
    /**
     * Sets a Tower on this square. Square is marked as not available.
     *
     * @param tower The Tower to add to this square.
     */
    public void setTower(Tower tower) {
        this.tower = tower;
        available = false;
    }
    
    /**
     * Returns the Tower on this square.
     *
     * @return The Tower on this square.
     */
    public Tower getTower() {
        return this.tower;
    }
    
    /**
     * Returns true if there is no Tower on this square.
     *
     * @return true if there is no Tower on this square, false otherwise.
     */
    public boolean isAvailable() {
        return this.available;
    }
}