package se.umu.cs.dit06ajnajs.level;

import java.awt.Image;

import se.umu.cs.dit06ajnajs.agent.Direction;
import se.umu.cs.dit06ajnajs.agent.Unit;
import java.util.List;
import java.util.ArrayList;
import java.awt.Graphics;
import java.util.logging.Logger;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 * Class TurnSquare extends PathSquare and represents a square with a turn.
 * Contains a List with legal directions and a variable containing the current
 * direction that the unit is given when it is turned
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @version 1.0
 */
public class TurnSquare extends PathSquare {
    private static Logger logger = Logger.getLogger("AntiTD");

    private List<Direction> directions;
    private Direction currentDirection = Direction.NONE;

    /**
     * Creates a new TurnSquare, calls extended MapSquares contructor with
     * supplied parameters.
     * 
     * @param x The x-coordinate of this Square.
     * @param y The y-coordinate of this Square.
     * @param image The Image used to display this Square.
     */
    public TurnSquare(int x, int y, Image image) {
        super(x, y, image);
    }

    /**
     * Setup Directions, this field is unique for every TurnSquare.
     */
    @Override
    public void init() {
        this.directions = new ArrayList<Direction>(4);
    }

    /**
     * Add a possible Direction to this TurnSquare. The currentDirection will be
     * changed to this new Direction.
     *
     * @param direction The Direction to add.
     */
    public void addDirection(Direction direction) {
        logger.info("Added direction: " + direction + " to: " + this);
        this.currentDirection = direction;
        if (!directions.contains(direction)) {
            directions.add(direction);
        }
    }

    /**
     * Toggles current direction. Sets current direction to next direction among
     * possible directions in this TurnSquare.
     */
    private void toggleDirections() {
        int index = directions.indexOf(currentDirection);
        int nextIndex = (index + 1) % directions.size();
        // TODO, verify. Should get next direction from list.
        this.currentDirection = directions.get(nextIndex);
    }

    /**
     * On call, the direction is toggled
     */
    @Override
    public void click() {
        toggleDirections();
    }

    /**
     * Calls super.landon(unit) and changes landing Units position if its center
     * point has passed this TurnSquares center point.
     *
     * @param unit The Unit who lands on this TurnSquare.
     */
    @Override
    public void landOn(Unit unit) {
        super.landOn(unit);
        //TODO continue
        int unitCenterX = (int) unit.getCenterPoint().getX();
        int unitCenterY = (int) unit.getCenterPoint().getY();

        switch(unit.getDirection()) {
        case UP:
            if (unitCenterY <= this.getCenterY()) {
                turnUnit(unit);
            }
            break;
        case DOWN:
            if (unitCenterY >= this.getCenterY()) {
                turnUnit(unit);
            }
            break;
        case LEFT:
            if (unitCenterX <= this.getCenterX()) {
                turnUnit(unit);
            }
            break;
        case RIGHT:
            if (unitCenterX >= this.getCenterX()) {
                turnUnit(unit);
            }
            break;
        default:
            // TODO: Error?
            throw new Error("Unit hasn't got any Direction");
        }
    }

    /**
     * Calls super.pain(p) and rotates the Image used to draw this TurnSquare to
     * match the current Direction of this square. Painted Images should always
     * point right when not rotated.
     *
     * @param g The Graphics object to paint to.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;

        switch (currentDirection) {
        case UP:
            g2.rotate(-Math.PI / 2, getCenterX(), getCenterY());
            break;
        case DOWN:
            g2.rotate(Math.PI / 2, getCenterX() , getCenterY());
            break;
        case LEFT:
            g2.rotate(Math.PI, getCenterX(), getCenterY());
            break;
        case RIGHT:
            // Images should always point right
            break;
        case NONE:
            // If this is called from LevelEditor
            break;
        default:
            // TODO: Error
            throw new Error("No direction in TurnSquare");
        }
        g2.drawImage(getImage(), getX(), getY(), null);
        // Reset Transform
        g2.setTransform(new AffineTransform());
    }

    /**
     * Turns the supplied Unit.
     *
     * @param unit The Unit to turn.
     */
    private void turnUnit(Unit unit) {
        unit.setDirection(currentDirection, this);
    }
}
