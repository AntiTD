package se.umu.cs.dit06ajnajs.level;

import se.umu.cs.dit06ajnajs.agent.Unit;

/**
 * Interface Traversable contains method for manipulate a unit
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @version 1.0 
 */
public interface Traversable {
    /**
     * Manipulates the unit provided
     * @param unit The unit provided
     */
    public void landOn(Unit unit);
}
