package se.umu.cs.dit06ajnajs;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import se.umu.cs.dit06ajnajs.agent.AgentPrototypeFactory;
import se.umu.cs.dit06ajnajs.agent.Tower;
import se.umu.cs.dit06ajnajs.agent.Unit;
import se.umu.cs.dit06ajnajs.level.GoalSquare;
import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.level.StartSquare;
import se.umu.cs.dit06ajnajs.util.SoundPlayer;
import se.umu.cs.edu.jap.highscoreservice.Entry;
import se.umu.cs.edu.jap.highscoreservice.HighScoreServiceClient;
import se.umu.cs.edu.jap.highscoreservice.stubs.FailureFaultException;

/**
 * The controller of this AntiTD game. Given a list of levels this class creates
 * and starts everyting needed to run this game. Two Threads are started to to
 * run and control the game.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class ATDController {
    private static Logger logger = Logger.getLogger("AntiTD");

    private final int FRAMES_PER_SECOND = 20;

    private ATDModel model;
    private ATDView view;
    
    private Thread animationThread;
    private Thread creditThread;
    
    private boolean paused;
    
    /**
     * Creates a model and a view according to the MVC design pattern. The model
     * gets a list of Levels and the View will be aware of the model so that it
     * can fetch updates from it. A new game is started directly.
     *
     * @param levels A list containg the Levels to use in this game.
     */
    public ATDController(List<Level> levels) {
        // Create model and view
        this.model = new ATDModel(levels);
        this.view = new ATDView(model);
        connectListeners();
        
        newGame();

        this.animationThread = new Thread(new AnimationThread());
        this.creditThread = new Thread(new CreditThread());
        animationThread.start();
        creditThread.start();
    }

    /**
     * Takes the game to next Level. Musictrack is changed. If there is no other
     * Level the game is won.
     */
    public void advanceLevel() {
        SoundPlayer.killMusic();
        if (model.advanceLevel()) {
            view.updateScoreboard();
            view.updateBackgroundImage();
            SoundPlayer.playMusic(model.getNextTrack());
        } else {
            // Game won
            winGame();
        }
    }

    /**
     * Creates a new game. Resets old information (if present) and starts the
     * first Level.
     */
    public void newGame() {
        SoundPlayer.killMusic();
        model.newGame();
        view.updateScoreboard();
        view.updateBackgroundImage();
        view.repaintGame();
        SoundPlayer.playMusic(model.getNextTrack());
    }
    
    /**
     * Restarts a Level. Resets credit and score to initial values when current
     * Level was started.
     */
    public void restartLevel() {
        SoundPlayer.killMusic();
        model.restartLevel();
        view.updateScoreboard();
        view.updateBackgroundImage();
       SoundPlayer.playMusic(model.getNextTrack());
    }

    /**
     * Game is won. Promts user for username and stores a highscore entry to a
     * HighScoreService.
     */
    public void winGame() {
        SoundPlayer.killMusic();
        try {
            String urlString = "http://salt.cs.umu.se:37080/axis2/services/HighScoreService";
            URL url = new URL(urlString);
            HighScoreServiceClient client = new HighScoreServiceClient(url);
            
            String name = view.promtForHighScoreEntry();
            String date = new Date().toString();
            String score = model.getScore() + "";
            
            client.store(new Entry(name, date, score));
            view.printMessage("Score stored.");
            newGame();
        } catch (FailureFaultException e) {
            view.printMessage("Couldn't store score");
        } catch (MalformedURLException e) {
            // TODO - fix error message
            e.printStackTrace();
        }
    }
    
    /**
     * Game is lost user is presented with option to start a new game or quit
     * this application.
     */
    public void loseGame() {
        SoundPlayer.killMusic();
        if (view.showLevelLostDialog() == 0) {
            newGame();
        } else {
            quitApplication();
        }
    }

    /**
     * Wrapper method, calls clickPoint(int x, int y).
     *
     * @param point The point to click.
     */
    public void clickPoint(Point point) {
        clickPoint(point.x, point.y);
    }


    /**
     * Clicks all Clickables at the specified x, y point.
     *
     * @param x The x-location to click.
     * @param y The y-location to click.
     */
    public void clickPoint(int x, int y) {
        List<Unit> units = model.getUnits();
        for (Unit unit : units) {
            if (unit.contains(x, y)) {
                unit.click();
            }
        }
        model.getCurrentLevel().getMapSquareAtPoint(x, y).click();
    }
    
    /**
     * Toggles sound on/off.
     */
    private void toggleMute() {
        if (SoundPlayer.isMute()) {
            SoundPlayer.setMute(false);
            view.updateMuteMenu("Mute");
            view.printMessage("Sound is on");
        } else {
            SoundPlayer.setMute(true);
            view.updateMuteMenu("unMute");
            view.printMessage("Sound is muted");
        }
    }
    
    /**
     * Toggles pause, which freezes game.
     *
     * TODO: Show a pause-screen, transparent grey image over the GameComponent.
     */
    private void togglePause() {
        if (paused) {
            ATDController.this.paused = false;
            view.updatePauseMenu("Pause");
            view.printMessage("");
        } else {
            ATDController.this.paused = true;
            view.updatePauseMenu("Resume");
            view.printMessage("Game is paused");
        }
    }
    
    /**
     * Quits this application.
     */
    private void quitApplication() {
        logger.info("Closing program");
        System.exit(0);
    }


    /**
     * AnimationThread is used to update the game state and repaint information
     * at a rapid rate. Code is running in an infinite loop and sleeps for the
     * specified milliseconds in constant FRAMES_PER_SECOND.
     */
    private class AnimationThread implements Runnable {
        private long usedTime;

        /**
         * While running, the Thread loops through all actions the game can
         * generate as many times per second determined by the final class
         * variable FRAMES_PER_SECOND
         */
        public void run() {
            while (true) {
                // Check if game is paused
                while (paused) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // Should not happen
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
                long startTime = System.currentTimeMillis();

                // Update all units
                List<Unit> units = model.getUnits();
                List<Unit> deadUnits = new ArrayList<Unit>();
                for (Unit unit : units) {
                    if (unit.isAlive()) {
                        // Collisioncheck
                        for (Unit otherUnit : units) {
                            if (unit != otherUnit) {
                                if (unit.intersectsNextMove(otherUnit)) {
                                    // Collision!
                                    unit.collision(otherUnit);
                                }
                            }
                        }
                        unit.act();
                    } else {
                        // Add dead unit
                        deadUnits.add(unit);
                        logger.info("Dead unit is collected to list deadUnits");
                        //TODO 
                        /*the towers should be rewarded for dead units
                          Remember that cleared units are also "dead" 
                          but should not be counted
                        */
                    }
                }
                
                // Collect dead units
                if (!deadUnits.isEmpty()) {
                    model.removeUnits(deadUnits);
                    logger.info("Dead agents cleared");
                }
                
                // Update all towers
                List<Tower> towers = model.getTowers();
                for (Tower tower : towers) {
                    tower.act();
                }

                // Remove units from goalsquares and count points
                GoalSquare[] goalSquares = model.getGoalSquares();
                int credit = 0;
                int numOfUnits = 0;
                for (GoalSquare square : goalSquares) {
                    numOfUnits += square.getNumUnits();
                    credit += square.getCredit();
                }
                // Only update model if changes has been made and level is not
                // completed
                if ((credit > 0 || numOfUnits > 0) 
                    && !model.isLevelComplete()) {
                    model.addCredit(credit);    
                    model.addGoalUnit(numOfUnits);
                    view.updateScoreboard();
                    SoundPlayer.play(model.getSound("goal"));
                }
                
                // Release Unit from StartSquares
                StartSquare[] startSquares = model.getStartSquares();
                for (StartSquare startSquare : startSquares) {
                    Unit unitToStart = startSquare.getUnitToStart();
                    boolean unitToStartCollided = false;
                    if (unitToStart != null) {
                        for (Unit unit : units) {
                            unitToStartCollided = unitToStart.intersects(unit);
                            if (unitToStartCollided) {
                                // Collision
                                break;
                            }
                        }
                        if (!unitToStartCollided) {
                            // No collision found
                            startSquare.removeUnit(unitToStart);

                            model.releaseUnit(unitToStart);
                        }
                    }
                }

                // Repaint all agents
                Graphics g = view.getGameGraphics();
                for (Unit unit : units) {
                    unit.paint(g);
                }
                for (Tower tower : towers) {
                    tower.paint(g);
                }

                // Refresh the game view
                view.repaintGame();

                this.usedTime = System.currentTimeMillis() - startTime;
                // Try to keep a given number of frames per second.
                try {
                    //System.out.println("usedTime: >" + usedTime + "<");
                    long waitTime 
                        = ((1000 - usedTime) / FRAMES_PER_SECOND);
                    waitTime = (waitTime < 0) ? 0 : waitTime;
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    // If game is slow 1000 - usedTime can be negative
                    logger.warning("Missed frame");
                }
            }
        }
    }
    
    /**
     * While running, the thread sleeps for an interval and then calculates
     * the earned credits from units on the map. The credits are then added
     * to the player
     */
    private class CreditThread implements Runnable {
        public CreditThread() {

        }

        /**
         * While running, the thread sleeps for an interval and then calculates
         * the earned credits from units on the map. The credits are then added
         * to the player
         */
        public void run() {
            while (true) {
                // Check if game is paused
                while(paused) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // Should not happen
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
                // Calculate earned credits
                int credit = calculateCredit();
                if (credit > 0) {
                    model.addCredit(credit);
                    view.updateScoreboard();
                }
                // Goal check
                if (model.isLevelComplete()) {
                    //System.out.println("Victory!");
                    SoundPlayer.killMusic();
                    SoundPlayer.play(model.getSound("victory"));
                    // Check if user wants to restart level or go to next
                    if(view.showLevelCompleteDialog() == 0) {
                        advanceLevel();
                    } else {
                        restartLevel();
                    }
                }
                // Lose check
                if (model.isLevelLost()) {
                    
                    //System.out.println("You lose!");
                    loseGame();
                }
                try {
                    // Sleep an interval
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Method to calculate total credit earned. Every unit on Level gives a
         * score of 10.
         *
         * @return The total creadit earned from units currently existing in
         * Level.
         */
        private int calculateCredit() {
            int credit = model.getUnits().size() * 10;            
            return credit;
        }
    }

    /**
     * Connect listeners to View
     */
    private void connectListeners() {
        this.view.addMapListener(new MapListener());
        this.view.addClosingListener(new ClosingListener());
        this.view.addReleaseUnitListener(new ReleaseUnitListener());
        this.view.addPauseMenuItemListener(new PausResumeListener());
        this.view.addMuteMenuItemListener(new MuteListener());
        this.view.addNewGameMenuItemListener(new NewGameListener());
        this.view.addAboutMenuItemListener(new AboutListener());
        this.view.addHelpMenuItemListener(new HelpListener());
        this.view.addRestartLevelMenuItemListener(new RestartLevelListener());
    }

    /* Inner Listener classes ****************************************/

    /**
     * Should listen to GameComponent in View and run method click() on every
     * clickable item at the position clicked.
     */
    private class MapListener extends MouseAdapter {
        /**
         * Clicks every clickble at MouseEvents x-, y-position.
         *
         * @param me The Event that invoked this method.
         */
        @Override
        public void mouseReleased(MouseEvent me) {
            clickPoint(me.getX(), me.getY());
            // TODO: Only update what is needed.
            view.updateBackgroundImage();
        }
    }

    /**
     * Listenes for program to close. 
     */
    private class ClosingListener extends WindowAdapter
    implements ActionListener {
        /**
         * Used quitMenuItem, quits the program.
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            quitApplication();
        }

        /**
         * Used to detect when window is closed, quits this application.
         *
         * @param we a <code>WindowEvent</code> value
         */
        @Override
        public void windowClosing(WindowEvent we) {
            quitApplication();
        }
    }

    /**
     * Listener to listen for PausResume events.
     */
    private class PausResumeListener implements ActionListener {
        /**
         * Toggles paus/resume
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            ATDController.this.togglePause();
        }
    }

    /**
     * Listener to listen for Mute/unMute events.
     */
    private class MuteListener implements ActionListener {
        /**
         * Toggle mute/unmute
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            ATDController.this.toggleMute();
        }
    }

    /**
     * Listener to listen for new game events.
     */
    private class NewGameListener implements ActionListener {
        /**
         * Creates a new game.
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            ATDController.this.newGame();
        }
    }
    
    /**
     * Listener to listen for restart level events.
     */
    private class RestartLevelListener implements ActionListener {
        /**
         * Restarts Level.
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            ATDController.this.restartLevel();
        }
    }
    
    /**
     * Listener invoked when a user whants to read the information about the
     * game.
     */
    private class AboutListener implements ActionListener {
        /**
         * Pauses game and shows an about dialog.
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            if (!paused) {
                ATDController.this.togglePause();
            }
            view.showAboutDialog();
            ATDController.this.togglePause();
        }
    }
    
    /**
     * Listener invoked when a user whants to read the information about how the
     * game is played.
     */
    private class HelpListener implements ActionListener {
        /**
         * Pauses game and shows a help dialog.
         *
         * @param ae Not used.
         */
        public void actionPerformed(ActionEvent ae) {
            if (!paused) {
                ATDController.this.togglePause();
            }
            view.showHelpDialog();
            ATDController.this.togglePause();
        }        
    }

    /**
     * Listener to listen for a user to want to add a new Unit in the game.
     */
    private class ReleaseUnitListener implements ActionListener {
        /**
         * Creates a new unit and adds it to the game model if the current
         * player have sufficient fund.
         *
         * @param ae an <code>ActionEvent</code> value
         */
        public void actionPerformed(ActionEvent ae) {
            AgentPrototypeFactory factory = AgentPrototypeFactory.getInstance();
            if (!model.addUnit(factory.createUnit(view.getSelectedUnitType()))) {
                view.printMessage("Insufficient funds!");
            } else {
                view.printMessage("");
            }
        }
    }
}