package se.umu.cs.dit06ajnajs.agent;

import java.applet.AudioClip;
import java.awt.Image;
import java.util.Map;

/**
 * A very basic Unit, uses the all methods from the abstract class Unit.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class FootmanUnit extends Unit {
    /**
     * Creates a new <code>FootmanUnit</code> instance.
     *
     * @param type The type or name of this Unit, this is used as a unique
     * identifier in AgentPrototypeFactory to create different Units from this
     * class.
     * @param width The width of this Unit.
     * @param height The height of this Unit.
     * @param speed The speed of this Unit. Unit with specified pixels every
     * timeframe.
     * @param health The health of this Unit.
     * @param cost The cost of this Unit.
     */
    public FootmanUnit(String type, int width, int height, int speed, int health,
                       int cost, Map<Direction, Image> images, 
                       Map<String,AudioClip> sounds) {        
        super(type, width, height, speed, health, cost, images, sounds);
    }
}
