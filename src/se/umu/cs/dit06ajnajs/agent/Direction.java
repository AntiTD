package se.umu.cs.dit06ajnajs.agent;

/**
 * Enumeration used to represent different possible directions in the game.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public enum Direction {
    /**Upwards on screen*/ UP,
    /**Downwards on screen*/ DOWN,
    /**Left on screen*/ LEFT,
    /**Right on screen*/ RIGHT,
    /**No direction*/ NONE;
}
