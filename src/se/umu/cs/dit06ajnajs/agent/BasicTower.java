package se.umu.cs.dit06ajnajs.agent;

import java.awt.Image;

/**
 * A basic implementation which extends the abstract class Tower. This class is
 * used to instantiate Tower.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class BasicTower extends Tower {

    /**
     * Creates a new <code>BasicTower</code> instance.
     *
     * @param width The width of this Tower.
     * @param height The height of this Tower.
     * @param damage The amount of damage this Tower gives each time it fires.
     * @param range The range of this Tower given as a radius of pixels around
     * the tower.
     * @param image The Image that this tower draws when its paint() method is
     * called.
     */
    public BasicTower(int width, int height, int damage, int range, Image image) {
        super(width, height, damage, range, image);
    }
}