package se.umu.cs.dit06ajnajs.agent;

import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Map;
import java.util.logging.Logger;

import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.level.MapSquare;
import se.umu.cs.dit06ajnajs.level.Traversable;
import se.umu.cs.dit06ajnajs.util.SoundPlayer;

/**
 * Abstract class used as superclass to all Units in a game. All methods are
 * implemented and can be used as a basic Unit.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public abstract class Unit implements Agent {
    private static Logger logger = Logger.getLogger("AntiTD");

    private int x;
    private int y;
    private int centerX;
    private int centerY;

    private int width;
    private int height;

    private String type;
    private int speed;
    private int health;
    private int currentHealth;
    private int cost;

    private Map<Direction, Image> images;
    private Map<String, AudioClip> sounds;
    private Direction direction;
    private Level level;
    private boolean alive;
    private boolean pause;
    private Object lastTurnedBy;
        
    /**
     * Instantiates all supplied parameters. Sets this Unit to be alive and not
     * paused.
     *
     * @param type The type or name of this Unit, this is used as a unique
     * identifier in AgentPrototypeFactory to create different Units from this
     * class.
     * @param width The width of this Unit.
     * @param height The height of this Unit.
     * @param speed The speed of this Unit. Unit with specified pixels every
     * timeframe.
     * @param health The health of this Unit.
     * @param cost The cost of this Unit.
     */
    public Unit(String type, int width, int height, int speed, int health,
                int cost, Map<Direction, Image> images, 
                Map<String, AudioClip> sounds) {
        this.type = type;
        this.width = width;
        this.height = height;
        setX(x);
        setY(y);
        this.speed = speed;
        this.health = health;
        this.currentHealth = health;
        this.cost = cost;
        this.images = images;
        this.sounds = sounds;

        this.alive = true;
        this.pause = false;
    }

    /**
     * Sets the image the Paintable should contain.
     * @param img The image a Paintable should contain.
     */
    public void setImage(Image img) {
        // TODO, change Paintable to Map<Comparable, Image>
    }
    
    /**
     * Returns the type/name of this Unit.
     *
     * @return The type/name of this Unit.
     */
    public String getType() {
        return this.type;
    }
    
    /**
     * When it's time for this Unit to act, this method is called. It checks
     * current health to see if it died. Otherwise it calculates next position
     * and moves there by changing it's x and y coordinates. If the new
     * possition is on a Traversable MapSquare it calles it's landOn method,
     * otherwise it dies. If the unit is paused it does not move, but sets pause
     * back to true, this is the case if there is a collision in next
     * move. Collsions are set from a controller class.
     */
    public void act() {
        // Check if this unit has health left
        if (this.currentHealth > 0) {
            // Check if this unit is active
            if (!pause) {
                Point nextPos = getNextPosition();
                // TODO check for collision on next position
                move(nextPos);
                
                // Land on current square
                MapSquare currentSquare
                    = level.getMapSquareAtPoint(centerX, centerY);
                if (currentSquare instanceof Traversable) {
                    ((Traversable) currentSquare).landOn(this);
                } else {
                    // Unit hitting something not Traversable and therefore
                    // dies.
                    this.alive = false;
                }
            } else {
                // Toggle pause back to false;
                pause = false;
            }
        } else {
            // Kill unit
            this.alive = false;
            SoundPlayer.play(sounds.get("dead"));
        }
    }

    /**
     * Calculates and returns the next position this unit will have after next
     * move.
     *
     * @return The new position.
     */
    public Point getNextPosition() {
        switch (direction) {
        case UP:
            logger.fine("UP");
            return new Point(x, y - speed);
        case DOWN:
            logger.fine("DOWN");
            return new Point(x, y + speed);
        case LEFT:
            logger.fine("LEFT");
            return new Point(x - speed, y);
        case RIGHT:
            logger.fine("RIGHT");
            return new Point(x + speed, y);
        default:
            System.err.println("Unit has not got a valid Direction");
            // TODO, make a NoValidException and throw
            return null;
        }
    }

    /**
     * Move Unit to specified Point. New x and y coordinates are set.
     *
     * @param point The Point to move to.
     */
    public void move(Point point) {
        setX(point.x);
        setY(point.y);
    }

    /** Used to calculate and set a new x-center after x is changed. */
    private void recalculateCenterX() {
        this.centerX = this.x + this.width / 2;
    }
    
    /** Used to calculate and set a new y-center after y is changed. */
    private void recalculateCenterY() {
        this.centerY = this.y + this.height / 2;
    }
    
    /**
     * Returns the current x value.
     *
     * @return The current x value.
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the current x value, recalculates and sets a new centerX value.
     *
     * @param x The new x value.
     */
    public void setX(int x) {
        this.x = x;
        recalculateCenterX();
    }

    /**
     * Returns the current y value.
     *
     * @return The current y value.
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the current y value, recalculates and sets a new centerY value.
     *
     * @param y The new y value.
     */
    public void setY(int y) {
        this.y = y;
        recalculateCenterY();
    }

    /**
     * Sets the current width, recalculates and sets a new centerX value.
     *
     * @param width The new width value.
     */
    public void setWidth(int width) {
        this.width = width;
        recalculateCenterX();
    }

    /**
     * Returns the current width.
     *
     * @return The current width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the current height, recalculates and sets a new centerY value.
     *
     * @param height The new height value.
     */
    public void setHeight(int height) {
        this.height = height;
        recalculateCenterY();
    }

    /**
     * Returns the current height.
     *
     * @return The current height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets a new centerX position. Recalculates and sets the new x-position.
     *
     * @param centerX The new centerX position.
     */
    public void setCenterX(int centerX) {
        this.centerX = centerX;
        setX(centerX - (width / 2));
    }
    
    /**
     * Sets a new centerY position. Recalculates and sets the new y-position.
     *
     * @param centerY The new centerY position.
     */
    public void setCenterY(int centerY) {
        this.centerY = centerY;
        setY(centerY - (width / 2));
    }

    /**
     * Sets a new center point. Recalculates and sets the new x- and  y-position.
     *
     * @param centerX The new centerX position.
     * @param centerY The new centerY position.
     */
    public void setCenterPoint(int centerX, int centerY) {
        setCenterPoint(new Point(centerX, centerY));
    }

    /**
     * Sets a new center point. Recalculates and sets the new x- and  y-position.
     *
     * @param point The new center point.
     */
    public void setCenterPoint(Point point) {
        setCenterX(point.x);
        setCenterY(point.y);
    }
    
    /**
     * Returns the center point.
     *
     * @return The center Point.
     */
    public Point getCenterPoint() {
        return new Point(this.centerX, this.centerY);
    }

    /**
     * Sets a new speed. Could be used by Items to give Units speedboosts and
     * slowdowns.
     *
     * @param speed The new speed.
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    
    /**
     * Returns the speed of this Unit.
     *
     * @return The speed of this Unit.
     */
    public int getSpeed() {
        return speed;
    }


    /**
     * Returns the cost of this Unit.
     *
     * @return The cost of this Unit.
     */
    public int getCost() {
        return this.cost;
    }

    /**
     * Sets the Direction of this Unit. If this method is called by the same
     * Object twice in a row the new Direction is not accepted. This is used to
     * prevent Units from dislocation in TurnSquares.
     *
     * @param direction The new Direction.
     * @param caller The caller.
     */
    public void setDirection(Direction direction, Object caller) {
        if (this.lastTurnedBy != caller) {
            this.direction = direction;
            this.lastTurnedBy = caller;
        } else {
            logger.info("Turned by same caller twice: " + caller);
        }
    }
    
    /**
     * Returns the Direction of this Unit.
     *
     * @return The Direction of this Unit.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Set a Level for this Unit.
     *
     * @param level The Level this Unit exists in.
     */
    public void setLevel(Level level) {
        this.level = level;
    }

    /**
     * Sets this Units alive status.
     *
     * @param state false if Unit should be killed, true if it should live.
     */
    public void setAlive(boolean state) {
        this.alive = state;
    }
    
    /**
     * Checks if this Unit is alive.
     *
     * @return true if Unit is alive, false otherwise.
     */
    public boolean isAlive() {
        return this.alive;
    }

    /**
     * Decrease the units currentHealth.
     * @param damage The number of health points to substract
     */
    public void damage(int damage) {
        this.currentHealth -= damage;
    }

    /**
     * Attemts to clone this Unit. Used by AgentPrototypeFactory to create new
     * Units.
     *
     * @return A new instance of the same type as the instantiated Unit.
     */
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new Error("Object " + this.getClass().getName()
                            + " is not Cloneable");
        }
    }
    
    /**
     * Called if there is a collision in this Units next position. If this Unit
     * collides with a Unit moving in opposite Direction they both bounce,
     * otherwise this Unit pauses next time it's act() method is called.
     *
     * @param unit The Unit this unit collides with.
     */
    public void collision(Unit unit) {
        switch (direction) {
        case UP:
            if (unit.getDirection() == Direction.DOWN) {
                setDirection(Direction.DOWN, unit);
                unit.setDirection(Direction.UP, this);
                return;
            }
            break;
        case DOWN:
            if (unit.getDirection() == Direction.UP) {
                setDirection(Direction.UP, unit);
                unit.setDirection(Direction.DOWN, this);
                return;
            }
            break;
        case LEFT:
            if (unit.getDirection() == Direction.RIGHT) {
                setDirection(Direction.RIGHT, unit);
                unit.setDirection(Direction.LEFT, this);
                return;
            }
            break;
        case RIGHT:
            if (unit.getDirection() == Direction.LEFT) {
                setDirection(Direction.LEFT, unit);
                unit.setDirection(Direction.RIGHT, this);
                return;
            }
            break;
        default:
            // TODO: create exception
            System.err.println("Unit hasn't got a valid Direction");
            break;
        }
        this.pause = true;
    }

    /**
     * Returns the state of pause.
     *
     * @return true if Unit is paused, false otherwise.
     */
    public boolean isPaused() {
        return this.pause;
    }

    /**
     * Overrides standard toString() method to return information about this
     * Unit.
     *
     * @return A String containing type, cost and health.
     */
    @Override
    public String toString() {
        return getType() + ", cost: " + getCost() + ", strenght: " + this.health;
    }
    
    /**
     * Method to handle mouseClicks, plays sound "onClick" from Map. Override in
     * subclass to implement special behaviour.
     */
    public void click() {
        logger.info("Unit clicked " + this.toString());
        SoundPlayer.play(sounds.get("onClick"));
    }
    
    /**
     * Checks if the specified x and y position is inside of this Units bounds.
     *
     * @param x The x-value to check.
     * @param y The y-value to check.
     * @return If the specified x and y position is inside of this Units bounds.
     */
    public boolean contains(int x, int y) {
        return new Rectangle(this.x, this.y, width, height).contains(x, y);
    }
    
    /**
     * Returns the bounding box this Unit exists in.
     *
     * TODO: if width != height the currentDirection needs to be checked.
     *
     * @return A rectangle with x, y, width, height of this Unit.n
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }
    
    /**
     * Returns the Units future bounding box.
     *
     * TODO: if width != height the currentDirection needs to be checked.
     *
     * @return A rectangle with x, y, width, height of this Units next position.
     */
    public Rectangle getFutureBounds() {
        Point nextPoint = getNextPosition();
        return new Rectangle(nextPoint.x, nextPoint.y, width, height);
    }
    
    /**
     * Checks if this supplied Unit intersects with this Unit.
     *
     * @param unit The Unit to check for intersection.
     * @return true if supplied Unit intersects with this Unit, false otherwise.
     */
    public boolean intersects(Unit unit) {
        return this.getBounds().intersects(unit.getBounds());
    }
    
    /**
     * Checks if this supplied Unit intersects with this Unit in its future
     * position.
     *
     * @param unit The Unit to check for intersection.
     * @return true if supplied Unit intersects with this Unit in its future,
     * false otherwise.
     */
    public boolean intersectsNextMove(Unit unit) {
        return this.getFutureBounds().intersects(unit.getBounds());
    }
    
    /**
     * Paints this Unit on the supplied Graphics. Draws Units Image, and a
     * healthbar displaying current health.
     *
     * @param g The Graphics to paint to.
     */
    public void paint(Graphics g) {
        Image image = images.get(direction);
        g.drawImage(image, x, y, null);
        
        // Paint health bar
        int healthHeight = 2;
        int yHealth = y + height - healthHeight;
        g.setColor(Color.RED);
        g.fillRect(x , yHealth, width, healthHeight);
        g.setColor(Color.GREEN);
        int healthBar = (int) (width * (new Double(currentHealth) / health));
        g.fillRect(x , yHealth, healthBar, healthHeight);
    }
}
