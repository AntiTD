package se.umu.cs.dit06ajnajs.agent;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;
import java.awt.Rectangle;

/**
 * Abstract class used as superclass to all Towers in a game. All methods are
 * implemented and can be used as a basic Tower.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public abstract class Tower implements Agent, Observer {

    private static Logger logger = Logger.getLogger("AntiTD");
    
    private int x;
    private int y;
    private int centerX;
    private int centerY;

    private int width;
    private int height;

    private int damage;
    private int range;
    private Image image;
    private boolean alive;
    private boolean fiering;
    private List<Unit> fireingList;

    /**
     * Sets basic parameters needed for a Tower, sets the Tower to be alive and
     * not fiering.
     *
     * @param width The width of this Tower.
     * @param height The height of this Tower.
     * @param damage The amount of damage this Tower gives each time it fires.
     * @param range The range of this Tower given as a radius of pixels around
     * the tower.
     * @param image The Image that this tower draws when its paint() method is
     * called.
     */
    public Tower(int width, int height, int damage, int range, Image image) {
        this.width = width;
        this.height = height;
        this.damage = damage;
        this.range = range;
        this.image = image;
        
        this.alive = true;
        this.fiering = false;
    }

    /**
     * Used to initialize fields that should not be individually set for every
     * Tower, since clone() is shallow.
     */
    public void init() {
        this.fireingList = new ArrayList<Unit>();        
    }
    
    /**
     * Set the image used by this Tower.
     *
     * @param image A new Image.
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * Sets the position of this Tower.
     *
     * @param point The new point of this Tower.
     */
    public void setPostition(Point point) {
        this.x = point.x;
        this.y = point.y;
        this.centerX = this.x + (this.width/2);
        this.centerY = this.y + (this.height/2);
    }

    /**
     * Get the range of this Thower.
     *
     * @return The range of this Tower.
     */
    public int getRange() {
        return this.range;
    }

    /**
     * Called when this Tower acts. If it's fireing list is not empty it fires
     * on Units that are in range and removes Units that are out of range.
     */
    public void act() {
        if (!fireingList.isEmpty()) {
            Unit unit = fireingList.get(0);
            if (unit.isAlive()) {
                Point unitPoint = unit.getCenterPoint();

                int unitXPos = unitPoint.x;
                int unitYPos = unitPoint.y;
                int distans = (int) Math.hypot((this.centerX - unitXPos),
                                               this.centerY - unitYPos);
                logger.fine("Tower range: " + this.range);
                logger.fine("Distance to unit: " + distans);
                if (distans < this.range) {
                    logger.fine("UNIT IN RANGE, FIIIIIREEEEEEE!!!");
                    this.fiering = true;
                    unit.damage(damage);
                } else {
                    logger.info("Unit out of reach, removing from fireingList");
                    fireingList.remove(unit);
                }
            } else {
                logger.info("Unit is dead, removing from fireingList");
                fireingList.remove(unit);
            }
        }
    }

    /**
     * Towers are added to PathSquares in their fireingrage, when Units landOn()
     * those squares this method is called. Units from argument to this method
     * are added to firering list.
     *
     * @param caller The caller of this method.
     * @param arg Should always be the Unit traversing over a PathSquares in
     * range from this Tower.
     */
    public void update(Observable caller, Object arg) {
        // Should always be units
        if (arg instanceof Unit) {
            Unit unit = (Unit) arg;
            if (!fireingList.contains(unit)) {
                fireingList.add(unit);
                logger.info("Unit >" + unit.getClass().getSimpleName() + 
                "< added to tower...");
            }
        }
    }

    /**
     * Returns the state of this Tower, dead or alive.
     *
     * @return true if this Tower is alive, false otherwise.
     */
    public boolean isAlive() {
        return this.alive;
    }

    /**
     * Sets this Tower alive status.
     *
     * @param state false if Tower should be killed, true if it should live.
     */
    public void setAlive(boolean state) {
        this.alive = state;
    }

    /** Used to calculate and set a new x-center after x is changed. */
    private void recalculateCenterX() {
        this.centerX = this.x + this.width / 2;
    }
    
    /** Used to calculate and set a new y-center after y is changed. */
    private void recalculateCenterY() {
        this.centerY = this.y + this.height / 2;
    }
    
    /**
     * Sets the current height, recalculates and sets a new centerY value.
     *
     * @param height The new height value.
     */
    public void setHeight(int height) {
        this.height = height;
        recalculateCenterY();
    }

    /**
     * Returns the height of this Tower.
     *
     * @return The  height of this Tower.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the width of this Tower.
     *
     * @return The width of this Tower.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width, recalculates and sets a new centerX value.
     *
     * @param width The new width value.
     */
    public void setWidth(int width) {
        this.width = width;
        recalculateCenterX();
    }

    /**
     * Returns the x-position of this Tower.
     *
     * @return The x-position of this Tower.
     */
    public int getX() {
        return this.x;
    }

    /**
     * Returns the y-position of this Tower.
     *
     * @return The y-position of this Tower.
     */
    public int getY() {
        return this.y;
    }

    /**
     * Returns the x-center point of this Tower.
     *
     * @return  The x-center point of this Tower.
     */
    public int getCenterX() {
        return centerX;
    }

    /**
     * Returns the y-center point of this Tower.
     *
     * @return  The y-center point of this Tower.
     */
    public int getCenterY() {
        return centerY;
    }
    
    /**
     * Get the center point of this Tower.
     *
     * @return The center point of this Tower.
     */
    public Point getCenterPoint() {
        return new Point(this.centerX, this.centerY);
    }
    
    /**
     * Returns the state of this Towers fireing.
     *
     * @return true if this Tower fires, false otherwise.
     */
    private boolean isFiering() {
        return this.fiering;
    }
    
    /**
     * Set the fire state of this Tower.
     *
     * @param fiering true if the Tower is fireing, false otherwise.
     */
    private void setFiering(boolean fiering) {
        this.fiering = fiering;
    }
    
    /**
     * Returns a list of Units this Tower has in its fireing list.
     *
     * @return A list of Units this Tower has in its fireing list.
     */
    public List<Unit> getFireingList() {
        return this.fireingList;
    }
    
    /**
     * Paints this Tower.
     *
     * @param g The Graphics to paint to.
     */
    public void paint(Graphics g) {
        g.drawImage(image, x, y, null);
        
        // Draw a line to the Unit this Tower fires on.
        if (this.isFiering()) {
            this.setFiering(false);
            g.setColor(Color.WHITE);
            List<Unit> fireingList = this.getFireingList();
            if (!fireingList.isEmpty()) {
                Unit unit = this.getFireingList().get(0);
                g.drawLine(this.getCenterX(), this.getCenterY(),
                           unit.getCenterPoint().x, unit.getCenterPoint().y);
            }
        }
    }
    
    /**
     * Empty method to handle mouseClicks, override in subclass to implement
     * behaviour.
     */
    public void click() {
        // Override in subclass to implement behaviour.
    }

    /**
     * Checks if the specified x and y position is inside of this Towers bounds.
     *
     * @param x The x-value to check.
     * @param y The y-value to check.
     * @return If the specified x and y position is inside of this Towers bounds.
     */
    public boolean contains(int x, int y) {
        return new Rectangle(this.x, this.y, width, height).contains(x, y);
    }
    
    /**
     * Attemts to clone this Tower.
     *
     * @return A new instance of the same type as the instantiated Tower.
     */
    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new Error("Object " + this.getClass().getName()
                            + " is not Cloneable");
        }
    }
}
