package se.umu.cs.dit06ajnajs.agent;

import se.umu.cs.dit06ajnajs.Clickable;
import se.umu.cs.dit06ajnajs.Paintable;

/**
 * Interface for every class that acts in this game.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public interface Agent extends Paintable, Clickable, Cloneable {
    /**
     * Updates this Agent.
     */
    public void act();

    /**
     * @return True if the agent is alive
     */
    public boolean isAlive();
}