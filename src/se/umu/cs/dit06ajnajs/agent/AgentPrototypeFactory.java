package se.umu.cs.dit06ajnajs.agent;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import javax.imageio.ImageIO;

import se.umu.cs.dit06ajnajs.AntiTD;

/**
 * AgentPrototypeFactory is used to create new instances of different
 * implementations of agents from a String. Implemented as a Singleton.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class AgentPrototypeFactory {
    private static final AgentPrototypeFactory INSTANCE
        = new AgentPrototypeFactory();
    private java.util.Map<String, Unit> unitMap;
    private java.util.Map<String, Tower> towerMap;
    private int lowestCost;

    /**
     * Gets the only instance of this class, if none exists one is created.
     *
     * @return The only instance of this class.
     */
    public static AgentPrototypeFactory getInstance() {
        return INSTANCE;
    }

    /**
     * AgentPrototypeFactory initialices all different types of Units and Towers
     * and puts them in a java.util.Map.
     *
     * Private constructor makes sure there can only be one instance of this
     * class, and that instance is created by the class itself from the first
     * invocation of getInstance()
     *
     */
    private AgentPrototypeFactory() {
        unitMap = new HashMap<String, Unit>();
        towerMap = new HashMap<String, Tower>();

        // Instantiate Units and Towers
        try {
            // Get directory containing images
            // TODO: move to config class of file
            URL imagesURL = 
                this.getClass().getResource("/resources/unit-images/");
            URL soundsURL = getClass().getResource("/resources/sounds/");
            
            /*
             *  Init Units
             */
            
            /* Create Standard FootmanUnit */
            // Images
            HashMap<Direction, Image> images = new HashMap<Direction, Image>();
            BufferedImage image = ImageIO.read(
                    new URL(imagesURL, "footmanDown.gif"));
            images.put(Direction.DOWN, image);
            image = ImageIO.read(new URL(imagesURL, "footmanUp.gif"));
            images.put(Direction.UP, image);
            image = ImageIO.read(new URL(imagesURL, "footmanLeft.gif"));
            images.put(Direction.LEFT, image);
            image = ImageIO.read(new URL(imagesURL, "footmanRight.gif"));
            images.put(Direction.RIGHT, image);
            
            // Sounds
            HashMap<String, AudioClip> sounds = 
                new HashMap<String, AudioClip>();
            AudioClip sound = Applet.newAudioClip(
                    new URL(soundsURL, "dead.wav"));
            sounds.put("dead", sound);
            sound = Applet.newAudioClip(new URL(soundsURL, "footman.wav"));
            sounds.put("onClick", sound);
            
            unitMap.put("Footman",
                        new FootmanUnit("Footman", 40, 40, 3, 400, 500, 
                                images, sounds));
            
            /* Create Seal */
            // Images
            images = new HashMap<Direction, Image>();
            image = ImageIO.read(new URL(imagesURL, "sealDown.gif"));
            images.put(Direction.DOWN, image);
            image = ImageIO.read(new URL(imagesURL, "sealUp.gif"));
            images.put(Direction.UP, image);
            image = ImageIO.read(new URL(imagesURL, "sealLeft.gif"));
            images.put(Direction.LEFT, image);
            image = ImageIO.read(new URL(imagesURL, "sealRight.gif"));
            images.put(Direction.RIGHT, image);
            
            // Sounds
            sounds = new HashMap<String, AudioClip>();
            sound = Applet.newAudioClip(new URL(soundsURL, "sealdead.wav"));
            sounds.put("dead", sound);
            sound = Applet.newAudioClip(new URL(soundsURL, "seal.wav"));
            sounds.put("onClick", sound);
            
            unitMap.put("Seal",
                        new FootmanUnit("Seal", 33, 33, 2, 200, 200, 
                                images, sounds));

            /*
             *Init Towers
             */
            imagesURL = this.getClass().getResource("/resources/tower-images/");
            
            /* Create Basic Tower */
            image = ImageIO.read(new URL(imagesURL, "basicTower.gif"));
            towerMap.put("BasicTower",
                         new BasicTower(41, 41, 3,
                                        (int) (AntiTD.SQUARE_SIZE*1.5),
                                        image));
            
        } catch (IOException e) {
            System.err.println("Couldn't find image. Exception: "
                               + e.getMessage());
        }

        // Find cost of cheapest unit
        this.lowestCost = Integer.MAX_VALUE;
        for (String s : getUnitTypes()) {
            Unit unit = createUnit(s);
            if (unit.getCost() < this.lowestCost) {
                this.lowestCost = unit.getCost();
            }
        }
    }

    /**
     * Create a new Unit based on the supplied String. Unit will be cloned from
     * a Map of Units.
     *
     * @param type The name of the type of Unit to create.
     * @return A new Unit.
     */
    public Unit createUnit(String type) {
        Unit unitPrototype = unitMap.get(type);
        if (unitPrototype == null) {
            throw new IllegalArgumentException("The specified type '" + type
                                               + "' doesn't exist");
        }
        Unit resultUnit = (Unit) unitPrototype.clone();
        return resultUnit;
    }

    /**
     * Create a Tower based on the supplied String. Tower will be cloned from a
     * Map of Towers.
     *
     * @param type The name of the type of Tower to create.
     * @return A new Tower.
     */
    public Tower createTower(String type) {
        Tower towerPrototype = towerMap.get(type);
        if (towerPrototype == null) {
            throw new IllegalArgumentException("The specified type '" + type
                                               + "' doesn't exist");
        }
        Tower resultTower = (Tower) towerPrototype.clone();
        resultTower.init();
        return resultTower;
    }

    /**
     * Override clone, to make sure that no subclass can clone this instance.
     *
     * @return Should not return anything, CloneNotSupportedException should be thrown.
     * @exception CloneNotSupportedException Is thrown since this class is not
     * supposed to be cloned().
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("AgentPrototypeFactory is a "
                                             + "Singleton no clones are"
                                             + " supported.");
    }

    /**
     * Returns a Set containing all Strings of the UnitTypes this Class can
     * produce.
     *
     * @return A Set containing all unitTypes this Class can produce.
     */
    public Set<String> getUnitTypes() {
        return unitMap.keySet();
    }
    
    /**
     * Returns int representing the cost for the cheapest unit 
     * @return int representing the cost for the cheapest unit
     */
    public int getLowestCost() {
        return this.lowestCost;
    }
}