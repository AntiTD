package se.umu.cs.dit06ajnajs;

import java.awt.Graphics;
import java.awt.Image;

/**
 * Interface for every class that should be able to paint itself to a Graphics
 * object.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public interface Paintable {
    /**
     * Paints this Paintable.
     * @param g The Graphics to paint with.
     */
    public void paint(Graphics g);
    
    /**
     * Sets the image the Paintable should contain.
     * @param img The image a Paintable should contain.
     */
    public void setImage(Image img);

    /**
     * @return The center point of the Agent.
     */
    public java.awt.Point getCenterPoint();
}
