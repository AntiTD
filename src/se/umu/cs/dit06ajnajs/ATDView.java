package se.umu.cs.dit06ajnajs;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import java.util.EventListener;
import javax.swing.JButton;
import javax.swing.DefaultListModel;

import se.umu.cs.dit06ajnajs.agent.AgentPrototypeFactory;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import se.umu.cs.dit06ajnajs.agent.Unit;
import javax.swing.SwingUtilities;

import se.umu.cs.dit06ajnajs.level.MapSquare;

/**
 * ATDView is the view of this game, it creates a JFrame and fills it with
 * menues and components that make up the entire interface of this game.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @author Andreas Jakobsson (dit06ajs@cs.umu.se)
 * @version 1.0
 */
public class ATDView { 
    private static Logger logger = Logger.getLogger("AntiTD");
    
    private ATDModel model;
    
    // Components
    private JFrame frame;
    private JTextArea scoreboard;
    private JLabel messageLabel;
    //private JLabel scoreboard;
    
    private GameComponent gameComponent;
    private Graphics2D gameGraphics;
    
    // Menu
    private JMenuItem newGameMenuItem;
    private JMenuItem restartLevelMenuItem;
    private JMenuItem pausMenuItem;
    private JMenuItem muteMenuItem;
    private JMenuItem quitMenuItem;
    private JMenuItem helpMenuItem;
    private JMenuItem aboutMenuItem;
    
    // GameController
    private JButton releaseUnitsButton;

    private JList unitList;
    
    /**
     * Creates a new <code>ATDView</code> instance. Saves the supplied model as
     * a field and calls createAndShowGUI().
     *
     * @param model The model used for this game. Used to get information from.
     */
    public ATDView(ATDModel model) {
        this.model = model;
        createAndShowGUI();
    }
    
    /**
     * Create and show GUI used by this application.
     */
    private void createAndShowGUI() {
        this.gameComponent = new GameComponent();
        
        JPanel mainPanel = new JPanel(new BorderLayout());
        
        mainPanel.add(createControlPanel(), BorderLayout.EAST);
        mainPanel.add(createMenu(), BorderLayout.NORTH);
        mainPanel.add(gameComponent, BorderLayout.CENTER);
        
        this.frame = new JFrame();
        frame.setTitle("AntiTD");
        frame.add(mainPanel);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Creates the JPanel containg Components to control the game.
     *
     * @return The JPanel containg Components to control the game.
     */
    private JPanel createControlPanel() {
        JPanel resultPanel = new JPanel(new BorderLayout());
        
        // JList unitTypes
        DefaultListModel unitTypesListModel = new DefaultListModel();
        AgentPrototypeFactory factory = AgentPrototypeFactory.getInstance();
        for (String type : factory.getUnitTypes()) {
            Unit unit = factory.createUnit(type);
            unitTypesListModel.addElement(unit);
        }
        this.unitList= new JList(unitTypesListModel);
        unitList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // Set one index as selected
        unitList.setSelectedIndex(0);
        resultPanel.add(unitList, BorderLayout.NORTH);

        JPanel shopPanel = new JPanel();
        
        shopPanel.setLayout(new BoxLayout(shopPanel, BoxLayout.Y_AXIS));
        // Scoreboard
        scoreboard = new JTextArea("Credit:\t0\nScore:\t0\nCleared Units:\t0/0",1,10);
        scoreboard.setOpaque(false);
        scoreboard.setEditable(false);
        
        // MessageArea
        this.messageLabel = new JLabel();
        resultPanel.add(messageLabel, BorderLayout.CENTER);
            
        // Release Button
        this.releaseUnitsButton = new JButton("Release Units");
        
        shopPanel.add(scoreboard);
        shopPanel.add(releaseUnitsButton);
        
        resultPanel.add(shopPanel, BorderLayout.SOUTH);
        return resultPanel;
    }
    
    /**
     * Creates the MenuBar used by this application.
     *
     * @return The MenuBar used by this application.
     */
    private JMenuBar createMenu() {
        JMenuBar menubar = new JMenuBar();
        
        // Menu AntiTD
        JMenu antiTDMenu = new JMenu("AntiTD");
        
        // TODO change name depending on if a game is running or not.
        this.newGameMenuItem = new JMenuItem("New Game");
        antiTDMenu.add(newGameMenuItem);
        
        this.restartLevelMenuItem = new JMenuItem("Restart level");
        antiTDMenu.add(restartLevelMenuItem);
        
        this.pausMenuItem = new JMenuItem("Pause");
        antiTDMenu.add(pausMenuItem);
        
        this.muteMenuItem = new JMenuItem("Mute");
        antiTDMenu.add(muteMenuItem);
        
        this.quitMenuItem = new JMenuItem("Quit");
        antiTDMenu.add(quitMenuItem);
        
        // Menu Help
        JMenu helpMenu = new JMenu("Help");
        
        this.helpMenuItem = new JMenuItem("Help");
        helpMenu.add(helpMenuItem);
        
        this.aboutMenuItem = new JMenuItem("About");
        helpMenu.add(aboutMenuItem);
        
        // Add menus to menubar
        menubar.add(antiTDMenu);
        menubar.add(helpMenu);
        return menubar;
    }

    /**
     * Returns the Graphics used to update visual information about Agents in
     * the game.
     *
     * @return The Graphics used by Agents on the Level.
     */
    public Graphics getGameGraphics() {
        return gameGraphics;
    }

    /**
     * Returns the type of the currently selected Unit.
     *
     * @return Type of the currently selected Unit.
     */
    public String getSelectedUnitType() {
        return ((Unit) this.unitList.getSelectedValue()).getType();
    }

    /**
     * Updates the backgroundimage used, for example when MapSquare are changed
     * during gameplay.
     */
    public void updateBackgroundImage() {
        this.gameComponent.updateBackgroundImage();
    }

    /**
     * Updates the backgroundimage with new information from supplied MapSquare,
     * for example when MapSquare are changed during gameplay.
     *
     * @param square A MapSquare with new informatin to be painted.
     */
    public void updateBackgroundImage(MapSquare square) {
        this.gameComponent.updateBackgroundImage(square);
    }
    
    /**
     * Calls repaint on the GameComponent.
     */
    public void repaintGame() {
        gameComponent.repaint();
    }

    /**
     * Update and repaint Pause menu information.
     */
     public void updatePauseMenu(final String TEXT) {
         SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     ATDView.this.pausMenuItem.setText(TEXT);
                 }
             });
     }

    /**
     * Update and repaint Pause menu information.
     */
     public void updateMuteMenu(final String TEXT) {
         SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     ATDView.this.muteMenuItem.setText(TEXT);
                 }
             });
     }
    
    /**
     * Prints a message to messageLabel, previous message is erased.
     */
     public void printMessage(final String TEXT) {
         SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     ATDView.this.messageLabel.setText(TEXT);                     
                 }
             });
     }

     /**
      * Updates the numbers on the scoreboard
      */
     public void updateScoreboard() {
         SwingUtilities.invokeLater(new Runnable() {
             public void run() {
                 scoreboard.setText("Credit:\t" + model.getCredit() + "\n" +
                        "Score:\t" + model.getScore() + "\n" + 
                        "Cleared units:\t" + model.getNumOfClearedUnits() 
                        + "/" + model.getUnitsToWin());
             }
         });
     }

    public String promtForHighScoreEntry() {
        String message = "Want to send in a highscore, what's your username?";
        String title = "Enter highscore";
        String result = JOptionPane.showInputDialog(
                frame, message, title, JOptionPane.PLAIN_MESSAGE);
        return result;
    }
     
    /**
     * Prompts user to play next level or restart current level
     * @return 0 for option next level
     * @return 1 for option restart current level
     */
     public int showLevelCompleteDialog() {
         String title = "Level completed";
         String message = "Congratulations! You have completed this level.";
         String[] options = new String[2];
         options[0] = "Next level";
         options[1] = "Restart level";

         int index = JOptionPane.showOptionDialog(frame,
                 message,
                 title,
                 JOptionPane.YES_NO_OPTION,
                 JOptionPane.QUESTION_MESSAGE,
                 null,     //do not use a custom Icon
                 options,  //the titles of buttons
                 options[0]); //default button title
         return index;
     }
     
     /**
      * Prompts user to start a new game or quit application
      * @return 0 for option new game
      * @return 1 for option quit application

      */
     public int showLevelLostDialog() {
         String title = "Level lost";
         String message = "You failed to complete the level.\n" +
                        "Play a new game or quit application";
         String[] options = new String[2];
         options[0] = "Play new game";
         options[1] = "Quit application";

         int index = JOptionPane.showOptionDialog(frame,
                 message,
                 title,
                 JOptionPane.YES_NO_OPTION,
                 JOptionPane.QUESTION_MESSAGE,
                 null,     //do not use a custom Icon
                 options,  //the titles of buttons
                 options[0]); //default button title
         return index;         
     }
     

     public void showAboutDialog() {
         // TODO Auto-generated method stub
         String title = "About";
         String message = "*** Anti-tower Defence (alfa) ***" +
                        "\n\nCreated by:\n" +
                        "Andreas Jakobsson (dit06ajs@cs.umu.se)\n" +
                        "Anton Johansson (dit06ajn@cs.umu.se)";
         JOptionPane.showMessageDialog(frame,
                 message,
                 title,
                 JOptionPane.INFORMATION_MESSAGE,
                 null);
     }
     
     public void showHelpDialog() {
         // TODO Auto-generated method stub
         String title = "Help";
         JTextArea message = new JTextArea("Instructions\n" +
                        "The goal for Anti-tower Defence is to release units " +
                        "from start squares in such a way that they reach goal " +
                        "squares without being killed by towers. When enough " +
                        "units has made it to goal squares you have completed " +
                        "the level\n",1,40);
         message.setOpaque(false);
         message.setEditable(false);
         message.setLineWrap(true);
         JOptionPane.showMessageDialog(frame,
                 message,
                 title,
                 JOptionPane.INFORMATION_MESSAGE,
                 null);
     }
     
    /**
     * GameComponent is the componenent on which the Level and all Units and
     * Towers is shown.
     */
    private class GameComponent extends JComponent {
        private Image backgroundImage;
        // TODO: Se if its possible to change to Image
        private BufferedImage gameImage;
        
        private int width;
        private int height;
        
        public GameComponent() {
            super();
            this.width = (int) model.getMapDimension().getWidth();
            this.height = (int) model.getMapDimension().getHeight();
            this.setPreferredSize(model.getMapDimension());
            
            // Background Image
            this.backgroundImage = model.getMapImage();

            // Game Image
            this.gameImage = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_ARGB);
            ATDView.this.gameGraphics = gameImage.createGraphics();
        }

        public void updateBackgroundImage() {
            this.backgroundImage = model.getMapImage();
        }

        public void updateBackgroundImage(MapSquare square) {
            Graphics g = this.backgroundImage.getGraphics();
            square.paint(g);
        }
        
        /**
         * Ovverrides standard paintComponent, used to only repaint information
         * about Unit and Tower information.
         *
         * @param g The Graphics used to update visual information.
         */
        @Override
        public void paintComponent(Graphics g) {
            logger.fine("paintComponent: " + Thread.currentThread().toString());
            
            // Draw backgroundImage
            g.drawImage(backgroundImage, 0, 0, null);
            
            // Draw gameImage which should contain updated information from
            // Controller
            g.drawImage(gameImage, 0, 0, null);
            
            // Clear gameImage image with a big transparent rectangle.
            Color transparent = new Color(0, 0, 0, 0);
            gameGraphics.setColor(transparent);
            gameGraphics.setComposite(AlphaComposite.Src);
            gameGraphics.fill(new Rectangle2D.Double(0, 0, width, height));
        }
    }

    /* Set Listener methods ******************************/

    // TODO: Good way to not cast and not be to specific?
    public void addClosingListener(EventListener el) {
        this.quitMenuItem.addActionListener((ActionListener) el);
        this.frame.addWindowListener((WindowListener) el);
    }
    
    // Set menu item listeners
    public void addNewGameMenuItemListener(ActionListener al) {
        this.newGameMenuItem.addActionListener(al);
    }
    
    public void addRestartLevelMenuItemListener(ActionListener al) {
        this.restartLevelMenuItem.addActionListener(al);
    }

    public void addPauseMenuItemListener(ActionListener al) {
        this.pausMenuItem.addActionListener(al);
    }

    public void addMuteMenuItemListener(ActionListener al) {
        this.muteMenuItem.addActionListener(al);
    }

    public void addHelpMenuItemListener(ActionListener al) {
        this.helpMenuItem.addActionListener(al);
    }
    
    public void addAboutMenuItemListener(ActionListener al) {
        this.aboutMenuItem.addActionListener(al);
    }
    
    // Controls
    public void addReleaseUnitListener(ActionListener al) {
        this.releaseUnitsButton.addActionListener(al);
    }
        
    public void addMapListener(MouseListener ml) {
    	gameComponent.addMouseListener(ml);
    }
}
