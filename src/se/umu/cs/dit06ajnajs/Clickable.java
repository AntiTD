package se.umu.cs.dit06ajnajs;

/**
 * Interface Clickable is an interface for acting upon mouse clicks.
 *
 * @author Anton Johansson (dit06ajn@cs.umu.se)
 * @version 1.0
 */
public interface Clickable { 

    /**
     * Reacts on mouse clicks
     */
    public void click();

    /**
     * Checks if the specified x and y position is inside of this Units bounds.
     *
     * @param x The x-value to check.
     * @param y The y-value to check.
     * @return If the specified x and y position is inside of this Units bounds.
     */
    public boolean contains(int x, int y);
}
