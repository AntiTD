package se.umu.cs.dit06ajnajs;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;
import org.jdom.JDOMException;

import se.umu.cs.dit06ajnajs.level.Level;
import se.umu.cs.dit06ajnajs.util.LevelsXMLParser;

/**
 * AntiTD loads either the default levels.xml file or a specified file
 * containing XML-information describing the levels to play. XML-information is
 * parsed and a List containing the resulted Levels are feeded to a newly
 * created ATDController.
 *
 * @author Anton Johansson, dit06ajn@cs.umu.se
 * @version 1.0
 */
public class AntiTD {
    // The square size to use in the game, should be the same size as the images
    // used for MapSquares.
    public static final int SQUARE_SIZE = 45;
    
    // An URL pointing to the default levels.xml file.
    private static final URL DEFAULT_LEVELS_XML
        = AntiTD.class.getResource("/resources/levels.xml");
    
    /**
     * Starts the game, parses and validates an XML-file containing Level
     * information. Creates a new ATDController.
     * 
     * @param args if an argument is given this is used as a reference to an
     * XML-file which should contain Level information. If no argument is given
     * a default XML-file is used.
     */
    public static void main(String[] args) {
        Logger.getLogger("AntiTD").setLevel(java.util.logging.Level.OFF);
        
        try {
            List<Level> levels;
            
            if (args.length == 1) {
                levels = LevelsXMLParser.parse(new File(args[0]));
            } else {
                levels = LevelsXMLParser.parse(DEFAULT_LEVELS_XML);
            }
            new ATDController(levels);
        } catch (IOException e) {
            System.err.println("Couldn't find specified file, argument should "
                               + "point to an XML-file containing level information");
            System.exit(-1);
        } catch (JDOMException e) {
            System.err.println("Error parsing XML-file containing level information: ");
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }
}