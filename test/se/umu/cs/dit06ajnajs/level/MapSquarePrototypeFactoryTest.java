/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package se.umu.cs.dit06ajnajs.level;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author anton
 */
public class MapSquarePrototypeFactoryTest {
    private MapSquarePrototypeFactory factory;
    
    public MapSquarePrototypeFactoryTest() {
    }

    @Before
    public void setUp() {
        this.factory = MapSquarePrototypeFactory.getInstance();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSingleton() {
        MapSquarePrototypeFactory factoryTmp =
            MapSquarePrototypeFactory.getInstance();
        assertSame(this.factory, factoryTmp);
    }
    
    @Test
    public void testcreateMapSquareForNull() {
        try {
            factory.createMapSquare("doesnt exist", 1, 1);
        } catch (IllegalArgumentException e) {
            return;
        }
        fail("IllegalArgumentException should have been thrown");
    }
    
    @Test
    public void testcreateMapSquare() {
        MapSquare s = new TowerSquare(1, 2, null);
        MapSquare ss = factory.createMapSquare("TowerSquare", 5, 6);
        assertNotSame(s, ss);
        assertEquals(1, s.getX());
        assertEquals(2, s.getY());
        assertEquals(5, ss.getX());
        assertEquals(6, ss.getY());
        
        assertTrue(s instanceof TowerSquare);
        assertTrue(ss instanceof TowerSquare);

        MapSquare p = factory.createMapSquare("PathSquare", 1, 2);
        MapSquare pp = factory.createMapSquare("PathSquare", 1, 2);
        assertTrue(p instanceof PathSquare);
        assertTrue(pp instanceof PathSquare);
        
        assertEquals(1, p.getX());
        assertEquals(2, p.getY());

        assertNotSame(p, pp);

        MapSquare b = factory.createMapSquare("BlockedSquare", 1, 2);
        MapSquare bb = factory.createMapSquare("BlockedSquare", 1, 2);
        assertTrue(b instanceof BlockedSquare);
        assertTrue(bb instanceof BlockedSquare);
    }
}
